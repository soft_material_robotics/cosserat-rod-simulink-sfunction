#include <sl_function/sl_func.h>
#include <iostream>
#include <ctime>

/**
 * @brief This application runs the rod simulation as Simulink would.
 * @details You can build your own application from this minimal example
 * without using Simulink. A visualisation is not planned for the future.
 *
 */
int main()
{
	// define the input struct to the simulation
	double p = 0;
	IN_type in;
	for (int iP = 0; iP < 3; iP++)
	{
		in.pressure[iP] = p;
	}

	// define parameters required for simulation
	PARAM_type param;
	param.isDynamic = 0;
	param.isHyperelastic = 0;
	for (int i = 0; i < 2; i++)
	{
		param.stiffness_scaling[i] = 1;
	}
	for (int i = 0; i < 4; i++)
	{
		param.damping_coeffs[i] = 1;
	}

	// define output struct for simulation
	OUT_type out;

	clock_t t;
	SL_start_func(); // assemble actuator and set up simulation

	// main simulation loop
	for (int count = 8; count < 10; count++)
	{
		in.pressure[0] += 0; // compute sum control input
		t = clock();
		SL_io_func(&in, &param, &out); // run single step of simulation
		t = clock() - t;

		// print some results
		std::cout << "IO Time: " << ((float)t) / CLOCKS_PER_SEC << std::endl;
		std::cout << "(";
		for (int i = 0; i < 3; i++)
		{
			std::cout << "\t" << out.EE[i]; // endeffector position
		}
		std::cout << ")" << std::endl;
	}
	SL_terminate_func(); // delete objects
	return 0;
}
