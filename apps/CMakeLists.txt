# Configuring the application to run
# Information about including MatlabLibs: 
# https://de.mathworks.com/matlabcentral/answers/868278-could-not-find-matlabengine-using-the-following-names-libmatlabengine
# set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pthread") # required for MatlabEngine
# find_package(Matlab REQUIRED)

# define a target name
set(TARGET_NAME "RunApplication")
set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)

# setup target

# Test the simulink interface as executable
# no matlab/simulink required
add_executable(${TARGET_NAME} app.cpp)
target_link_libraries(${TARGET_NAME} generic_sl_interface Eigen3::Eigen)
target_link_directories(${TARGET_NAME} PRIVATE /usr/local/lib)
target_include_directories(${TARGET_NAME} PUBLIC ../include/)
target_compile_features(${TARGET_NAME} PRIVATE cxx_std_11)
