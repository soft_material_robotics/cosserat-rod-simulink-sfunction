#ifndef ROD_PARAMETER_H
#define ROD_PARAMETER_H
#include <custom_math/timemanagement.h>

/**
 * @name Indices physical quantities in state vector. 
 * Use these instead of magic numbers to avoid major bugs in the code. 
 */
///@{
#define INDEX_RX 0
#define INDEX_RY 1
#define INDEX_RZ 2
#define INDEX_HI 3
#define INDEX_HJ 4
#define INDEX_HK 5
#define INDEX_H 6
#define INDEX_NX 7
#define INDEX_NY 8
#define INDEX_NZ 9
#define INDEX_MX 10
#define INDEX_MY 11
#define INDEX_MZ 12
#define INDEX_QX 13
#define INDEX_QY 14
#define INDEX_QZ 15
#define INDEX_WX 16
#define INDEX_WY 17
#define INDEX_WZ 18
///@}
#define ALPHA -0.3 //<! parameter of bdf-alpha scheme
#define TIMESTEP 0.01 //<! parameter of bdf-alpha scheme

/**
 * @file rod_parameter.h contains multiple useful defines and numeric parameters for the bdf-alpha scheme.
 * @brief (Warning: Most of the content in this namespace will be moved to different classes in later versions)
 * @todo Replace this with a material-class! Huge overhead in ODE/PDE by copying objects of this type.
*/
namespace RodParameter
{
    /**
    * @brief A struct representing parameters depending on material, geometric or other properties.
    */
    struct dep
    {
        double c0_;
        Eigen::Vector3d g =  9.81 * Eigen::Vector3d::UnitZ();
        Eigen::Matrix3d Kse = Matrix3d::Identity();
        Eigen::Vector3d Kse_e3 = Vector3d::UnitZ();
        Eigen::Matrix3d Kbt = Matrix3d::Identity();
        Eigen::Matrix3d Kse_inv = Matrix3d::Identity();
        Eigen::Matrix3d Kbt_inv = Matrix3d::Identity();
        Eigen::Matrix3d rhoJ = Matrix3d::Identity();
        Eigen::Matrix3d Bse = Matrix3d::Identity();
        Eigen::Matrix3d Bbt = Matrix3d::Identity();
        Eigen::Matrix3d inv_of_Kse_c0Bse = Matrix3d::Identity();
        Eigen::Matrix3d inv_of_Kbt_c0Bbt = Matrix3d::Identity();
        double rhoA = 0;           //!< = rho * A;
        Eigen::Vector3d rhoAg = Vector3d::Zero(); //!< = rho * A * g;

        friend std::ostream &operator<<(std::ostream &os, dep const &out)
        {
            return os << out.g << "\n\n"
                      <<"K_se:\n" << out.Kse << "\n\n"
                      <<"K_bt:\n" << out.Kbt << "\n\n"
                      <<"B_se:\n" << out.Bse << "\n\n"
                      <<"B_bt:\n" << out.Bbt << "\n\n"
                      <<"Inertia:\n" << out.rhoJ << "\n\n"
                      << "Numerical parameter c0: \n" << out.c0_ << "\n\n";
        }
    };

    /**
    * @brief A struct representing material parameters with default values (Ecoflex0050)
    * @param E_ young's modulus
    * @param G_ shearing modulus
    * @param rho_ density of rod material in kg/m^3
    * @param Bs_ shearing material damping
    * @param Be_ extension material damping
    * @param Bb_ bending material damping
    * @param Bt_ torsional material damping
    * @param extensional_scaling scale extensional stiffness 
    * @param bending_scaling scale bending stiffness
    */
    struct mat
    {
        double E_ = 80000;   //!< young's modulus
        double G_ = 80000/(2*(1+0.5));   //!< shearing modulus
        double rho_ = 1080; //!< density of rod material in kg/m^3
        double Bs_ = 1;     //!< shearing material damping
        double Be_ = 10;    //!< extension material damping
        double Bb_ = 0.01;  //!< bending material damping
        double Bt_ = 1;     //!< torsional material damping
        double extensional_scaling = 1; //!< scale extensional stiffness 
        double bending_scaling = 1; //!< scale bending stiffness
    };

    /**
    * @brief A struct representing the rod's geometric parameters
    * @param rad_ radius of rod in m
    * @param length_ length in reference configuration in m
    * @param A_ area of rod in m2
    * @param I_ 2nd moment of inertia
    * @param dt_ time step resolution in s
    * @param alpha_ bdf-alpha coefficient
    */
    struct geo
    {
        double rad_; 
        double length_;
        double A_;
        Matrix3d I_; 
        double dt_ = TIMESTEP;
        double alpha_ = ALPHA; 
    };
}

#endif