#ifndef COSSERAT_ROD_H
#define COSSERAT_ROD_H
#define NORMALIZE 0 // if active normalizing quaternions at each integration step

#include <sim_lib/rod_element.h>
#include <material_lib/material_collection.h>
#include <custom_math/commonmath.h>
#include <custom_math/timemanagement.h>
#include <custom_math/convexoptimization.h>
#include <custom_math/numericalpde.h>
using namespace CRL;

/** 
 * @brief Collection of implemented odes/pdes of Cosserat rods. 
 * Any additional odes and pdes may be inserted here.
*/
namespace RodEquations{

/**
 * @brief Ordinary differential equation (statics) of a Cosserat rod.
 * @param y_s_out Spatial derivative of the state space vector of i-th node of 
 * the rod. This is the output of the ode.
 * @param z_out Reference to the time dependent states at node i.
 * @param y Reference to the current state at node i
 * @param rod Reference to Rodelement object containing properties of the
 * ODE/PDE, i.e, stiffness, resolution, geometry.
*/
void cosseratRodOde(VectorXd &y_s_out, Map<VectorXd> z_out, Map<VectorXd> y, RodElement &rod)
{

    // Map<Vector3d> p1(&y[0]);
    RodParameter::dep par = rod.getParam();
    Map<Quaterniond> Q(&y[3]);
    Map<Vector3d> n(&y[7]);
    Map<Vector3d> m(&y[10]);

    // Refer to the state vector derivative by its components
    Map<Vector3d> p_s(&y_s_out[0]);
    Map<Quaterniond> Q_s(&y_s_out[3]);
    Map<Vector3d> n_s(&y_s_out[7]);
    Map<Vector3d> m_s(&y_s_out[10]);
    Map<Vector3d> q_s(&y_s_out[13]);
    Map<Vector3d> w_s(&y_s_out[16]);

    // init derivatives
    p_s = 1 * Vector3d::UnitZ();
    Q_s.coeffs() << 0, 0, 0, 0;
    n_s << Vector3d::Zero();
    m_s << Vector3d::Zero();
    q_s << Vector3d::Zero();
    w_s << Vector3d::Zero();
    z_out << VectorXd::Zero(12);

    Q.normalize(); // normalize coeffs of quaternion (correction step)
    Matrix3d R = Q.toRotationMatrix();
    double precision_orthogonality = 1e-8;
    if ((R.determinant() - 1) >= precision_orthogonality)
    {
        std::cout << "\n\n warning (cosserat_rod_quat.h): det(R)-1 ="
                  << R.determinant() - 1 << "exceeds precision of "<< precision_orthogonality
                  << std::endl;
                  std::cout << "Results may be inaccurate." <<std::endl;
    }
    Vector3d v;
    if(rod.isHyperelastic())
    {
        // init
        double v_[3] = {0,0,0}; // sought deformations
        double RTn_[3]; // projected inner forces
        MatrixXd::Map(RTn_, 3, 1) = transposeMultiply(R,n); // set output 
        ogden_neural_network(RTn_,v_); // exported Ogden-FFNN
        v = Vector3d(v_);
    }
    else
    {
        // Hard-coded constitutive law w/ no precurvature
        v = par.Kse_inv * transposeMultiply(R, n);
        v(2) += 1;
    }
    // linear bending relationship
    Vector3d u = par.Kbt_inv * transposeMultiply(R, m);
    Matrix3d R_s;
    // ODEs
    p_s = R * v;
    R_s = hat_postmultiply(R, u);

    // computation of change in orientation (quaternion)
    MatrixXd temp(4, 4);
    temp << 0, u(2), -u(1), u(0),
        -u(2), 0, u(0), u(1),
        u(1), -u(0), 0, u(2),
        -u(0), -u(1), -u(2), 0;
    Q_s.coeffs() = temp * Q.coeffs() / 2;

    n_s -= par.rhoAg;
    m_s -= p_s.cross(n);
    
    // Change of follower force caused by internal pressurisation
    if (rod.isActuated())
    {
        for (auto ac : rod.chamb_)
        {
            n_s += R_s * ac.getForce();
            m_s += R * ((v + u.cross(ac.getLever())).cross(ac.getForce()));
            m_s += R * (ac.getLever().cross(u.cross(ac.getForce())));
        }
    }
    
    q_s = Vector3d::Zero();
    w_s = Vector3d::Zero();

    //     // check if numerical errors occur
    assert(!p_s.hasNaN());
    assert(!R_s.hasNaN());
    assert(!n_s.hasNaN());
    assert(!m_s.hasNaN());
    //     // Output argument for variables with time derivatives
    z_out << Vector6d::Zero(), v, u;
}

/**
 * @brief Partial differential equation (dynamics) of a Cosserat rod.
 * @param y_s_out Spatial derivative of the state space vector of i-th node of 
 * the rod. This is the output of the ode.
 * @param z_out Reference to the time dependent states at node i.
 * @param y Reference to the current state at node i
 * @param z_h Reference to the past state at node i (time discretisation)
 * @param rod Reference to Rodelement object containing properties of the
 * ODE/PDE, i.e, stiffness, resolution, geometry.
*/
void cosseratRodPDE(VectorXd &y_s_out, Map<VectorXd> z_out, Map<VectorXd> y, Map<VectorXd> z_h, RodElement &rod)
{
    // Unpack state vector
    RodParameter::dep par = rod.getParam();
    Map<Quaterniond> Q(&y[3]);
    Map<Vector3d> n(&y[7]);
    Map<Vector3d> m(&y[10]);
    Vector3d q = Map<Vector3d>(&y[13]);
    Vector3d w = Map<Vector3d>(&y[16]);

    // Refer to the state vector derivative by its components
    Map<Vector3d> p_s(&y_s_out[0]);
    Map<Quaterniond> Q_s(&y_s_out[3]);
    Map<Vector3d> n_s(&y_s_out[7]);
    Map<Vector3d> m_s(&y_s_out[10]);
    Map<Vector3d> q_s(&y_s_out[13]);
    Map<Vector3d> w_s(&y_s_out[16]);

    Q.normalize();
    Matrix3d R = Q.toRotationMatrix();

    // Unpack history terms
    Map<Vector3d> q_h(&z_h[0]);
    Map<Vector3d> w_h(&z_h[3]);
    Map<Vector3d> v_h(&z_h[6]);
    Map<Vector3d> u_h(&z_h[9]);

    // compute velocities
    Vector3d q_t = par.c0_ * q + q_h;
    Vector3d w_t = par.c0_ * w + w_h;

    // Material constitutive equation
    Vector3d v;
    if(rod.isHyperelastic())
    {
        double v_[3] = {0,0,0}; // sought deformations
        double RTn_[3]; // projected inner forces
        double v_hist[3] = {v_h[0],v_h[1],v_h[2]}; // compatibility with FFNN
        
        MatrixXd::Map(RTn_, 3, 1) = transposeMultiply(R, n) - par.Bse * v_h; // set output 
        dynamic_ogden_neural_network(RTn_,v_); // exported nonlinear visco-elatic-FFNN
        v = Vector3d(v_);
    }
    else
    {
        // linear relationship
        v = par.inv_of_Kse_c0Bse * (transposeMultiply(R, n) + par.Kse_e3 - par.Bse * v_h);
    }
    // linear bending relationship
    Vector3d u = par.inv_of_Kbt_c0Bbt * (transposeMultiply(R, m) - par.Bbt * u_h);
    Vector3d v_t = par.c0_ * v + v_h;
    Vector3d u_t = par.c0_ * u + u_h;

    Matrix3d R_s;
    R_s.setZero();
    // ODEs
    p_s = R * v;
    R_s = hat_postmultiply(R, u);

    // computation of change in orientation (quaternion)
    MatrixXd temp(4, 4);
    temp << 0, u(2), -u(1), u(0),
        -u(2), 0, u(0), u(1),
        u(1), -u(0), 0, u(2),
        -u(0), -u(1), -u(2), 0;
    Q_s.coeffs() = temp * Q.coeffs() / 2;

    n_s = par.rhoA * R * (q_t + w.cross(q)) - par.rhoAg;
    m_s = R * (par.rhoJ * w_t + w.cross(par.rhoJ * w)) - p_s.cross(n);

    // Change of follower force caused by internal pressurisation
    if (rod.isActuated())
    {
        for (auto ac : rod.chamb_)
        {
            n_s += R_s * ac.getForce();
            m_s += R * ((v + u.cross(ac.getLever())).cross(ac.getForce()));
            m_s += R * (ac.getLever().cross(u.cross(ac.getForce())));
        }
    }

    // spatial derivative of local velocities derived by Young's theorem
    q_s = v_t - u.cross(q) + w.cross(v);
    w_s = u_t - u.cross(w);

    // check if numerical errors occure
    assert(!p_s.hasNaN());
    assert(!R_s.hasNaN());
    assert(!n_s.hasNaN());
    assert(!m_s.hasNaN());
    assert(!q_s.hasNaN());
    assert(!w_s.hasNaN());
    // Output argument for variables with time derivatives
    z_out << q, w, v, u;
}

}

#endif