#ifndef AIR_CHAMBER_H
#define AIR_CHAMBER_H
#include <iostream>
#include <Eigen/Dense>
using namespace Eigen;

/**
 * @brief Create objectes of this class an parse them within a 
 * std::vector<AirChamber> to the RodElement constructor.
 * 
*/
class AirChamber{
private:
double areaCrossSec_; //!< cross-sectional area of the rod
double relPressure_; //!< relative pressure p_rel = p_abs - p0 in [Pa]
Vector3d posInCrossSec_;//!< position of center in cross sectional ref. frame 
double volume_; //!< current volume of air chamber for thermodynamics
protected:

public:
AirChamber(); //!< default constr.

/**
 * @brief Creates an circular AirChamber object.
 * @param radius in [m]
 * @param posInCrossSec double array to define the position in cross-section
 * expressed in reference system \f$R(\cdot) \in SO(3)\f$ of the RodElement 
 * in [m].
 *  
*/
AirChamber(double radius, double posInCrossSec[2]); 
~AirChamber();

AirChamber& operator= (const AirChamber& asignChamb); //!< assign operator

/**
 * @brief Use this funtion to increase pressure within the AirChamber. You can
 * access the AirChamber of each RodElement via its member chamb_
*/
void setPressure(double &val);

/**
 * @name Functions for pressure dynamics
 * Feature has not been introduced yet. Coming up soon.
*/
///@{
void setVolume(double val = 0); //!< sets Volume (required for thermodynamics) 
/**
 * @brief Adds a finite volume during num. integration. 
 * This called in a subroutine during numerical integration of the rod. 
 * 
*/
void addPartialVolume(Vector3d vi, Vector3d ui, double ds);
double getVolume(); // feature not introduced yet
///@}

Vector3d getForce(); //!< computes forces from pressure and cross-sec. area

/**
 * @brief Return in-plane xyz-coordinates of centre of AirChamber expressed
 * in local reference frame \f$ R(s_i) \in SO(3) \f$.
 * 
*/
Vector3d getLever(); 
double getArea(); //!< return value of cross-sectional area of AirChamber object
Matrix3d getSteiner(); //!< computes Steiner w.r.t. the principal axis
void display(); //!< print some properties of the object.
};
#endif