#ifndef ROD_ELEMENT_H
#define ROD_ELEMENT_H
#include <iostream>
#include <vector>
#include <sim_lib/air_chamber.h>
#include <custom_math/timemanagement.h>
#include <sim_lib/rod_parameter.h>
/**
 * @brief This class implements a section of rod.
 * @details There are multiple constructors and future contributions aiming at
 * the integration of new actuator concepts should do so by adding more
 * overloads to the constructor.
 * Multiple objects of RodElement are assembled using the Manipulator class.
 * Each RodElement defines the geometric and material properties within the p
 * partial/ordinary differential equations (PDE).
 *
 * @todo Add material-class if rod_parameters.h is refactorred
 * */
class RodElement
{
private:
    bool isPressureActuated_; //!< triggers a pressure actuation in ode/pde
    bool isHyperelastic_;     //!< switches between linear-elastic and hyperelastic in ode/pde
    int nNodes_;              //!< spatial resolution of rod element in integer/(ref. length)

    /*! rod material parameters (will later be moved into material class)*/
    RodParameter::geo geo_;
    RodParameter::mat mat_;
    RodParameter::dep dep_;

protected:
public:
    RodElement(); //!< default constructor

    /**
     * @brief Constructs an actuator with pressurisable air chambers.
     * @details
     * @param length Reference length in m
     * @param radius Radius of circular cross-section in m
     * @param nNodes Number of nodes in spatial discretisation
     * @param isActuated unused parameter, will be removed in next release.
     * @param chamb This containes a vector of AirChamber objects. Note, that
     * the current implementation only supports axial-symmetric cross-sections.
     * Therefore, the position and area of each AirChamber has to be chosen so 
     * that this criteria is met. To access the STL container within the
     * simulation use the public member chamb_. 
     */
    RodElement(double length, double radius, int nNodes, bool isActuated, std::vector<AirChamber> chamb);
    
    /**
     * @brief Constructs a simple rod with circular cross-section.
     * @details
     * @param length Reference length in m
     * @param radius Radius of circular cross-section in m
     * @param nNodes Number of nodes in spatial discretisation
     * @param isActuated unused parameter, will be removed in next release.
     */
    RodElement(double length, double radius, int nNodes, bool isActuated);                                //!< unactuated rod element
    ~RodElement();

    /**
     * @brief Initialises all required parameters for numerical computation.
     * @details This has to be executed everytime a parameter in geo_ or mat_ 
     * is changed to computed parameters in dep_.
    */
    void initDepParam(); 
    bool isActuated(); //!< used in pde to apply internal forces and moments
    bool isHyperelastic(); //!< if true, material is computed via Ogden-Networks [1]

    /**
     * @brief Setting the RodElement to hyper-elastic leads to the computation
     * of nonlinear stress-strain relationships via Ogden-Neural-Networks.
     * @details For details refer to Bartholdt et al., RoboSoft, 2023. Note 
     * that only one Ogden network is provided. 
     * @param isHyperelastic If this is set false, a linear theory will be applied.
     */
    void setHyperelastic(bool isHyperelatic);
    double getRefLength(); //!< returns ref. length of RodElement in [m]
    int getResolution();   //!< return number of nodes for spatial discretisation
    void display();        //!< this function has been introduced during debugging

    /**
     * @brief Returns dependent parameters.
     * @todo This is used in ode/pde to get the required parameters. It creates
     * a copy of a rel. large object in every integration step and is therefore
     * computationally inefficient. Needs to be refactored!
    */
    RodParameter::dep getParam();

    /**
     * @brief Change material parameters of rod by accessing the member mat_.
     * @details  Requires reinitialisation of dependent parameters in dep_.
     * Call initDepParam() afterwards to do so.
    */
    RodParameter::mat &setMaterial();

    /**
     * @brief This stl container collects all AirChamber objects parsed to the
     * respective constructor. You can access the AirChamber objects to 
     * ,i.e., set the pressure within the chamber (actuation).
     */
    std::vector<AirChamber> chamb_; //<! pressurized air chambers
};
#endif