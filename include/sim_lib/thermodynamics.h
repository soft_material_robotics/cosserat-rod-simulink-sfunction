#ifndef THERMODYNAMICS_H
#define THERMODYNAMICS_H
#include <iostream>
#include <Eigen/Dense>
#include <sim_lib/rod_element.h>
using namespace Eigen;

/*! This class implements all functions related to thermodynamic computation. 
It is used as a member of the SMR object.
*/
class ThermoDynamics{
private:
    Vector3d vi_; //!< local strain v for node i
    Vector3d ui_; //!< local strain u for node i
    double ds_;   //!< spatial integration step 
protected:

public:

ThermoDynamics();
~ThermoDynamics();
void computeVolume(RodElement &rod, MatrixXd &Z); //!< computes volumes of all AirChamber objects

};
#endif