#ifndef MANIPULATOR_H
#define MANIPULATOR_H

#include <sim_lib/rod_element.h>
#include <sim_lib/thermodynamics.h>
#include <custom_math/timemanagement.h>
#include <vector>
#define nStates 19
#define nDeriv 12


using namespace CRL;
/**
 * @brief This class implements an assembled manipulator.
 * @details By adding RodElement objects, the state space matrix as member of 
 * this class is extended by N entries depending on the resolution defined in 
 * the RodElement object. Objects of this class are parsed to the main numeric
 * integration routine, which automatically gets information about the currend 
 * rods properties to compute the ode/pde. It also manages the time 
 * discretisation (BDF-alpha) presented in Till et al. with the private 
 * member time_scheme.
 * Also, loads to the last node of the Manipulator can be applied.
 * 
 * An example on how to assemble a manipulator is given in the documentation
 * of the repository.
 * 
 * @todo Extension to define boundary conditions in this class.
*/
class Manipulator
{
private:
    const double dt_ = TIMESTEP; //!< resolution of time domain
    const double alpha_ = ALPHA; //!< alpha parameter of bdf scheme
    std::vector<RodElement> rod_; //!< serially connected rods of manipulator   
    /**
     * @brief Realises the time discretisation proposed by Till et al., 2019
    */
    std::vector<TimeManagerBdfAlpha*> time_scheme_;
    
    /**
     * @brief Feature not introduced yet. 
    */
    ThermoDynamics thermodyn_; 


public:
    Manipulator(); //!< default constr.
    ~Manipulator();
    std::vector<double> s; //!< material coordinate
    std::vector<MatrixXd> Y_; //!< state matrix [r, R, n, m, q, w]
    std::vector<MatrixXd> Z_; //!< state for that time deriv exist [q, w, v, u]
    std::vector<MatrixXd> Z_h_; //!< history terms
    
    void addRodElement(RodElement rod); //!< Connect RodElement to the end of obj
    void initTimeManager(); //!< call this after solving initial static problem
    void updateTime(); //!< update history terms of bdf scheme
    void updateThermoDynamics();//!< computes thermodynamics states and laws
    std::vector<RodElement> getRodElements(); //!< returns assembled rods
    std::vector<RodElement>& RodElements(); //!< setter function for rod member
    int getSpatialRes(); //!< get number of nodes per rod
    

    MatrixXd state(); //!< returns concatenated state matrix of all rods
    MatrixXd laststate(); //!< return concat. Z = (q w v u) (memory of last state)
    MatrixXd history(); //!< returns concatenated Z_h terms of all rods
    Vector3d EE(); //!< returns end effector (tip) Position in ref frame
    Vector3d EEForceExternal = Vector3d::Zero(); //!< external force at tip of last rod
    Vector3d EEMomentExternal = Vector3d::Zero(); //!< external moment at tip of last rod
    void setEEForceExternal(double forceExternal[3]); //!<set external tip force 
    void setEEMomentExternal(double momentExternal[3]); //!<set external tip moment 

    void printState(); //!< console print for whole state (tidier formating)
    void printLastState();
    void printHistTerms(); //!< console print for whole history terms (tidier formating)
    void display(); //!< reveals constructed RodElements and AirChambers contained in this obj. in console
    };

#endif