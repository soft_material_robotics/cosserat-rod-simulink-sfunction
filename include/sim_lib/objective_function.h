#ifndef OBJECTIVE_FUNCTION_H
#define OBJECTIVE_FUNCTION_H
#define NUM_INT_METHOD_STATIC 0 // 0 - euler, 1 - rk4

#include <Eigen/Dense>
#include <custom_math/numericalpde.h>
#include <sim_lib/cosserat_rod_quat.h>

using namespace Eigen;
using namespace CRL;
using namespace RodEquations;

/* This template function implements the objective function which is target to
the optimisation problem solving the boundary value problem for the manipulator*/
template <bool is_dynamic>
VectorXd objFunc(VectorXd guess, Manipulator &SMR)
{
    // std::cout<<"\n\nObjFcn-Guess: \t" <<  guess.transpose() << std::endl; //debug
    VectorXd y0 = VectorXd::Zero(19);
    Quaterniond h = Quaterniond::Identity();
    Matrix3d R;
    y0 << 0, 0, 0, h.coeffs(), guess, Vector6d::Zero();
    // define first node of manipulator
    int iRod = 0;
    SMR.Y_[iRod] << y0;
    iRod++;
    // Numerically integrate the semi-discretized rod PDEs
    if (is_dynamic)
    {
        for (auto &rodEl : SMR.getRodElements())
        {
            // this implements jump in forces and moments before actuated segments
            if (rodEl.isActuated())
            {
                // pneumatic actuation
                R = Quaterniond(y0.block<4, 1>(3, 0)).toRotationMatrix();
                for (auto &iChamb : rodEl.chamb_)
                {
                    y0.block<3, 1>(7, 0) += R * iChamb.getForce();
                    y0.block<3, 1>(10, 0) += R * (iChamb.getLever().cross(iChamb.getForce()));
                }
            }
            //TimeBdfAlpha_SpaceEuler<cosseratRodPDE, 19, 12>(SMR.Y_[iRod], SMR.Z_[iRod], y0, rodEl.getRefLength(), SMR.Z_h_[iRod], rodEl);
            TimeBdfAlpha_SpaceRK2<cosseratRodPDE, 19, 12>(SMR.Y_[iRod], SMR.Z_[iRod], y0, rodEl.getRefLength(), SMR.Z_h_[iRod], rodEl);
            // initial state of next RodElement is last state of current RodElement
            y0 << SMR.Y_[iRod].rightCols(1);

            // this implements jump in forces and moments after actuated segments
            if (rodEl.isActuated())
            {
                // pneumatic actuation
                R = Quaterniond(y0.block<4, 1>(3, 0)).toRotationMatrix();
                for (auto &iChamb : rodEl.chamb_)
                {
                    y0.block<3, 1>(7, 0) -= R * iChamb.getForce();
                    y0.block<3, 1>(10, 0) -= R * (iChamb.getLever().cross(iChamb.getForce()));
                }
            }
            iRod++; // next rod in manipulator
        }
    }
    else
    {
        // perform integration over each RodElement
        for (auto &rodEl : SMR.getRodElements())
        {
            // this implements jump in forces and moments before actuated segments
            if (rodEl.isActuated())
            {
                // pneumatic actuation
                R = Quaterniond(y0.block<4, 1>(3, 0)).toRotationMatrix();
                for (auto &iChamb : rodEl.chamb_)
                {
                    y0.block<3, 1>(7, 0) += R * iChamb.getForce();                             // jump in force
                    y0.block<3, 1>(10, 0) += R * (iChamb.getLever().cross(iChamb.getForce())); // jump in moments
                }
            }
            // integrate spatial domain of RodElement (static)
            // euler<cosseratRodOde, 19, 12>(SMR.Y_[iRod], SMR.Z_[iRod], y0, rodEl.getRefLength(), rodEl);
            rungekutta2<cosseratRodOde, 19, 12>(SMR.Y_[iRod], SMR.Z_[iRod], y0, rodEl.getRefLength(), rodEl);
            //rungekutta4<cosseratRodOde, 19, 12>(SMR.Y_[iRod], SMR.Z_[iRod], y0, rodEl.getRefLength(), rodEl);

            // set state of 1st node of next rod equal to last of current
            y0 << SMR.Y_[iRod].rightCols(1);

            // this implements jump in forces and moments after actuated segments
            if (rodEl.isActuated())
            {
                // pneumatic actuation
                R = Quaterniond(y0.block<4, 1>(3, 0)).toRotationMatrix();
                for (auto &iChamb : rodEl.chamb_)
                {
                    y0.block<3, 1>(7, 0) -= R * iChamb.getForce();                             // jump in force
                    y0.block<3, 1>(10, 0) -= R * (iChamb.getLever().cross(iChamb.getForce())); // jump in moments
                }
            }
            iRod++; // next rod in manipulator
        }
    }
    // set appended last node in manipulator
    if (iRod == SMR.getRodElements().size() + 1)
        SMR.Y_.back() << y0;

    // std::cout<<"Set last node!\t"<<SMR.Y_.back().block<3,1>(12,0).transpose()<<std::endl;
    Vector3d nL_shooting = SMR.Y_.back().block<3, 1>(7, 0);  // SMR.Y_[iRod].block<3, 1>(12, 1);
    Vector3d mL_shooting = SMR.Y_.back().block<3, 1>(10, 0); // SMR.Y_[iRod].block<3, 1>(12, 1);

    SMR.updateThermoDynamics(); // computes volumes, thermodynamics laws etc. 
    // Values for force at the last node N-1
    Vector6d distal_error;
    if (is_dynamic)
    {
        distal_error << nL_shooting - SMR.EEForceExternal, mL_shooting - SMR.EEMomentExternal ; // initial load free configuration
    }
    else
    {
        distal_error << nL_shooting - SMR.EEForceExternal, mL_shooting - SMR.EEMomentExternal ; // initial load free configuration
    }

    return distal_error;
}
#endif