//Eigen is a popular and feature-rich C++ matrix math library
//Project Website: http://eigen.tuxfamily.org

#ifndef EIGENMATRIXTYPES_H
#define EIGENMATRIXTYPES_H

#include <Eigen/Dense>
#include <sim_lib/rod_element.h>


using namespace Eigen;

//Extra types
typedef Matrix<double, 6, 1> Vector6d;
typedef Matrix<double, 6, 6> Matrix6d;
typedef Matrix<double, 19,1> Vector19d; // testing a new type for states
#endif // EIGENMATRIXTYPES_H
