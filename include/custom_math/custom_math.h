/* Header file to include all necessary header functions
to perform the shooting method for abitrary pdes*/
#include <custom_math/commonmath.h>
#include <custom_math/numericaldifferentiation.h>
#include <custom_math/convexoptimization.h>
#include <custom_math/numericalintegration.h>
#include <custom_math/numericalpde.h>
#include <custom_math/timemanagement.h>
