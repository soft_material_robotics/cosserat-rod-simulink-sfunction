#ifndef NUMERICALPDE_H
#define NUMERICALPDE_H

#include <custom_math/eigenmatrixtypes.h>
#include <sim_lib/rod_element.h>
namespace CRL
{
    inline void correctHistoryTermsRungeKutta4(Map<VectorXd> z_out, Map<VectorXd> y, VectorXd ys)
    {
        /*! The local derivates v and u are not exactly correct after the RK4. 
        Their value is equal to the last node k3 computed by the integration 
        method. Therefore, using the mean discrete derivative ys, we recompute
        v and u by projection of dr/ds with quaternion h.*/ 
        Map<Vector3d> r(&y[0]);
        Map<Quaterniond> Q(&y[3]);
        Map<Quaterniond> Qs(&ys[3]);
        Matrix3d R = Q.toRotationMatrix();
        Quaterniond u_quat;
        u_quat.coeffs() = (Q.inverse()*Qs).coeffs()/2;
        z_out << R.transpose()*r, u_quat.coeffs().tail(3), Vector6d::Zero();
    }

    // A function pointer typedef for [y_s,z] = f(y)
    typedef void(AutonomousOdeZFuncRod)(VectorXd &, Map<VectorXd>, Map<VectorXd>, RodElement &); // overload for toolbox

    /*! Integrate rod ODE using Euler's method, while setting a z vector to solve initial conditions. */
    template <AutonomousOdeZFuncRod ODE, int statesize, int derivsize>
    inline void euler(
        MatrixXd &Y,
        MatrixXd &Z,
        VectorXd y0,
        double L,
        RodElement &rod)
    {
        assert(Y.col(0).size() == y0.size());
        Y.col(0) = y0;
        double ds = L / (rod.getResolution() - 1);
        // std::cout<< "\n\n--- New euler int:";
        // std::cout<< "\tds\t" << ds << "\n";
        // Euler's method
        VectorXd y_s = VectorXd::Zero(statesize);
        //assert(!isnanf(y0.norm()) || )
        for (int i = 0; i < rod.getResolution() - 1; i++)
        {
            ODE(y_s, Map<VectorXd>(&Z(0, i), derivsize), Map<VectorXd>(&Y(0, i), statesize), rod);
            Y.col(i + 1) = Y.col(i) + ds * y_s;

        }
        ODE(y_s, Map<VectorXd>(&Z(0, rod.getResolution() - 1), derivsize), Map<VectorXd>(&Y(0, rod.getResolution() - 1), statesize), rod);
    }

    /*! Integrate rod ODE using RK2 method, while setting a z vector to solve initial conditions. */
    template <AutonomousOdeZFuncRod ODE, int statesize, int derivsize>
    inline void rungekutta2(
        MatrixXd &Y,
        MatrixXd &Z,
        VectorXd y0,
        double L,
        RodElement &rod)
    {
        
        assert(Y.col(0).size() == y0.size());
        double ds = L / (double)(rod.getResolution() - 1);

        // RK2 method
        VectorXd k0, k1;
        VectorXd y1;
        VectorXd y_s = y0; // asign y_s the same dimension as y0, content irrelevant
        k0 = VectorXd::Zero(statesize);
        k1 = VectorXd::Zero(statesize);
        y1 = VectorXd::Zero(statesize);

        Y.col(0) = y0;

        for (int i = 0; i < rod.getResolution() - 1; i++)
        {
            ODE(k0, Map<VectorXd>(&Z(0, i), derivsize), Map<VectorXd>(&Y(0, i), statesize), rod);
            y1 = Y.col(i) + k0 * ds;
            ODE(k1, Map<VectorXd>(&Z(0, i + 1), derivsize), Map<VectorXd>(&y1(0), statesize), rod); 
            Y.col(i + 1) = Y.col(i) + (k0 + k1) * ds/2.0;
        }
        ODE(y_s, Map<VectorXd>(&Z(0, rod.getResolution() - 1), derivsize), Map<VectorXd>(&Y(0, rod.getResolution() - 1), statesize), rod); // this normalizes the last orientation

    }

    
    /*! Integrate rod ODE using RK4 method, while setting a z vector to solve initial conditions. */
    template <AutonomousOdeZFuncRod ODE, int statesize, int derivsize>
    inline void rungekutta4(
        MatrixXd &Y,
        MatrixXd &Z,
        VectorXd y0,
        double L,
        RodElement &rod)
    {
        
        assert(Y.col(0).size() == y0.size());
        double ds = L / (double)(rod.getResolution() - 1);

        // RK4  method
        VectorXd k0, k1, k2, k3;
        VectorXd y1, y2, y3;

        k0 = VectorXd::Zero(statesize);
        k1 = VectorXd::Zero(statesize);
        k2 = VectorXd::Zero(statesize);
        k3 = VectorXd::Zero(statesize);
        y1 = VectorXd::Zero(statesize);
        y2 = VectorXd::Zero(statesize);
        y3 = VectorXd::Zero(statesize);

        Y.col(0) = y0;
        VectorXd y_s = y0;
        for (int i = 0; i < rod.getResolution() - 1; i++)
        {
            // TODO: Handling history terms correctly (v, w)
            ODE(k0, Map<VectorXd>(&Z(0, i), derivsize), Map<VectorXd>(&Y(0, i), statesize), rod);
            y1 = Y.col(i) + k0*ds/2.0;
            ODE(k1, Map<VectorXd>(&Z(0, i), derivsize), Map<VectorXd>(&y1(0), statesize), rod);
            y2 = Y.col(i) + k1*ds/2.0;
            ODE(k2, Map<VectorXd>(&Z(0, i), derivsize), Map<VectorXd>(&y2(0), statesize), rod);
            y3 = Y.col(i) + k2*ds;
            ODE(k3, Map<VectorXd>(&Z(0, i), derivsize), Map<VectorXd>(&y3(0), statesize), rod);
            y_s = (k0 + 2.0*(k1 + k2) + k3)/6.0;
            Y.col(i + 1) = Y.col(i) + ds * y_s ;
            correctHistoryTermsRungeKutta4(Map<VectorXd>(&Z(0, i), derivsize), Map<VectorXd>(&Y(0, i), statesize), y_s);
        }
        ODE(y_s, Map<VectorXd>(&Z(0, rod.getResolution() - 1), derivsize), Map<VectorXd>(&Y(0, rod.getResolution() - 1), statesize), rod); // this normalizes the last orientation
    }

    // A function pointer typedef for [y_s,z] = f(y,z_h)
    typedef void(AutonomousPdeFuncRod)(VectorXd &, Map<VectorXd>, Map<VectorXd>, Map<VectorXd>, RodElement &); // overload for toolbox
    /*! Integrate a semi-discretized PDE using Euler's method.
        The PDE function must set time-differientiated variables in the z vector,
        and the history terms z_h are calculated elsewhere. */
    template <AutonomousPdeFuncRod PDE, int statesize, int derivsize>
    inline void TimeBdfAlpha_SpaceEuler(
        MatrixXd &Y,
        MatrixXd &Z,
        VectorXd y0,
        double L,
        MatrixXd &Z_h,
        RodElement &rod)
    {
        Y.col(0) = y0;
        double ds = L / (rod.getResolution() - 1);

        // Euler's method
        VectorXd y_s = y0;
        for (int i = 0; i < rod.getResolution() - 1; i++)
        {

            PDE(y_s, Map<VectorXd>(&Z(0, i), derivsize), Map<VectorXd>(&Y(0, i), statesize), Map<VectorXd>(&Z_h(0, i), derivsize), rod);
            Y.col(i + 1) = Y.col(i) + ds * y_s;
            // normalize coeffs of quaternion (correction step)
            Map<Quaterniond> ori(&Y.col(i + 1)[3]);
            ori.normalize();
        }
        PDE(y_s, Map<VectorXd>(&Z(0, rod.getResolution() - 1), derivsize),
            Map<VectorXd>(&Y(0, rod.getResolution() - 1), statesize),
            Map<VectorXd>(&Z_h(0, rod.getResolution() - 1), derivsize), rod);
    }


    /*! Integrate a semi-discretized PDE using RK2 method.
        The PDE function must set time-differientiated variables in the z vector,
        and the history terms z_h are calculated elsewhere. 
        
        Important note: RK4 would require missing history terms for intermediate
        nodes between i+1 and i.*/
    template <AutonomousPdeFuncRod PDE, int statesize, int derivsize>
    inline void TimeBdfAlpha_SpaceRK2(
        MatrixXd &Y,
        MatrixXd &Z,
        VectorXd y0,
        double L,
        MatrixXd &Z_h,
        RodElement &rod)
    {


        double ds = L / (rod.getResolution() - 1);
        // RK2 method
        VectorXd k0, k1;
        VectorXd y1;
        VectorXd y_s = y0; // asign y_s the same dimension as y0, content irrelevant
        k0 = VectorXd::Zero(statesize);
        k1 = VectorXd::Zero(statesize);
        y1 = VectorXd::Zero(statesize);

        Y.col(0) = y0;

        for (int i = 0; i < rod.getResolution() - 1; i++)
        {
            PDE(k0, Map<VectorXd>(&Z(0, i), derivsize), 
            Map<VectorXd>(&Y(0, i), statesize), 
            Map<VectorXd>(&Z_h(0, i), derivsize), rod);

            y1 = Y.col(i) + k0 * ds;
            PDE(k1, Map<VectorXd>(&Z(0, i + 1), derivsize), 
            Map<VectorXd>(&y1(0), statesize), 
            Map<VectorXd>(&Z_h(0, i + 1), derivsize), rod);
            
            Y.col(i + 1) = Y.col(i) + (k0 + k1) * ds/2.0;
        }
        PDE(y_s, Map<VectorXd>(&Z(0, rod.getResolution() - 1), derivsize),
            Map<VectorXd>(&Y(0, rod.getResolution() - 1), statesize),
            Map<VectorXd>(&Z_h(0, rod.getResolution() - 1), derivsize), rod);
    }
}   

#endif // NUMERICALPDE_H
