#ifndef TEST_ODE_H
#define TEST_ODE_H
#include <Eigen/Dense>
#include <sim_lib/rod_element.h>
using namespace Eigen;
class TestODE{
    private:

    public:
    TestODE();
    ~TestODE();
    static void step(VectorXd &y_s_out, Map<VectorXd> z_out, Map<VectorXd> y, RodElement &rod);
};
#endif