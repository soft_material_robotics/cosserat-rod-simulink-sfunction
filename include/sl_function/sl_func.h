#ifndef SL_FUNC_H
#define SL_FUNC_H
#ifdef __cplusplus
extern "C"{
#endif

#define TOTAL_RES 80
#define EXTRA_STORAGE 20


typedef struct{
    double pressure[3];
    double EEForceExternal[3];
    double EEMomentExternal[3];
} IN_type;

typedef struct{
    double damping_coeffs[4]; // shear, extension, bending, torsion
    double stiffness_scaling[2]; // scale extensional and bending stiffness
    int isDynamic;
    int isHyperelastic;

} PARAM_type;   

typedef struct{
    double external_sim_time;
    double EE[3];

    double rx[TOTAL_RES];
    double ry[TOTAL_RES];
    double rz[TOTAL_RES];

    double hi[TOTAL_RES]; // imag i, j, k
    double hj[TOTAL_RES];
    double hk[TOTAL_RES]; 
    double h[TOTAL_RES]; // real
    
    double nx[TOTAL_RES];
    double ny[TOTAL_RES];
    double nz[TOTAL_RES];

    double mx[TOTAL_RES];
    double my[TOTAL_RES];
    double mz[TOTAL_RES];

    double qx[TOTAL_RES];
    double qy[TOTAL_RES];
    double qz[TOTAL_RES];

    double wx[TOTAL_RES];
    double wy[TOTAL_RES];
    double wz[TOTAL_RES];

    double s[TOTAL_RES + EXTRA_STORAGE];
    double chamber_volume[3];
} OUT_type;

void SL_start_func();
void SL_io_func(IN_type* ext_sim_in, PARAM_type* param, OUT_type* ext_sim_out);
void SL_terminate_func();
void initSim(PARAM_type *param);
void setHyperelasticFix(PARAM_type *param);
void checkInputParameter(PARAM_type *param);
#ifdef __cplusplus
}
#endif

#endif