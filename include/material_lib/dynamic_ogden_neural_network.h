//
//  Academic License - for use in teaching, academic research, and meeting
//  course requirements at degree granting institutions only.  Not for
//  government, commercial, or other organizational use.
//
//  dynamic_ogden_neural_network.h
//
//  Code generation for function 'dynamic_ogden_neural_network'
//


#ifndef DYNAMIC_OGDEN_NEURAL_NETWORK_H
#define DYNAMIC_OGDEN_NEURAL_NETWORK_H

// Include files
#include <cstddef>
#include <cstdlib>

// Function Declarations
extern void dynamic_ogden_neural_network(const double x1[3], double b_y1[3]);

#endif

// End of code generation (dynamic_ogden_neural_network.h)
