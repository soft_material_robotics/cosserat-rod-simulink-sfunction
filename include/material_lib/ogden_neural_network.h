//
//  Academic License - for use in teaching, academic research, and meeting
//  course requirements at degree granting institutions only.  Not for
//  government, commercial, or other organizational use.
//
//  ogden_neural_network.h
//
//  Code generation for function 'ogden_neural_network'
//


#ifndef OGDEN_NEURAL_NETWORK_H
#define OGDEN_NEURAL_NETWORK_H

// Include files
#include <cstddef>
#include <cstdlib>

// Function Declarations
extern void ogden_neural_network(const double x1[3], double b_y1[3]);

#endif

// End of code generation (ogden_neural_network.h)
