#include <sim_lib/rod_element.h>

RodElement::RodElement()
{
}

RodElement::RodElement(double length, double radius, int nNodes, bool isActuated, std::vector<AirChamber> chamb)
{
    geo_.rad_ = radius;
    geo_.length_ = length;
    nNodes_ = nNodes;
    isPressureActuated_ = true;
    isHyperelastic_ = false; // default
    chamb_ = chamb;
    initDepParam();
}

RodElement::RodElement(double length, double radius, int nNodes, bool isActuated)
{
    geo_.rad_ = radius;
    geo_.length_ = length;
    nNodes_ = nNodes;
    isPressureActuated_ = isActuated;
    isHyperelastic_ = false;
    initDepParam();
}

RodElement::~RodElement()
{
}

bool RodElement::isActuated()
{
    return isPressureActuated_;
}

bool RodElement::isHyperelastic(){
    return isHyperelastic_;
}

void RodElement::setHyperelastic(bool isHyperelastic){
    isHyperelastic_ = isHyperelastic;
}

double RodElement::getRefLength(){
    return geo_.length_;
}

void RodElement::initDepParam()
{
    dep_.c0_ = CRL::TimeManagerBdfAlpha::getC0(geo_.dt_, geo_.alpha_);
    dep_.g = 9.81*Vector3d::UnitZ();
    // compute required geometric parameters (circular cross section)
    geo_.A_ = M_PI * pow(geo_.rad_, 2);
    geo_.I_ = Matrix3d::Zero();
    geo_.I_.diagonal() =  M_PI * pow(geo_.rad_, 4) / 4 * Vector3d::Ones();
    geo_.I_.diagonal()[2] *= 2; // polar 2nd area of momentum
     
    if(isPressureActuated_){
        // for this to be accurate, the resulting center of area has to be 
        // x = 0 && y = 0
        int count_chamber = 0;
        for(auto chamb: chamb_){
            count_chamber++;
            geo_.A_ -= chamb.getArea();
            geo_.I_ += chamb.getSteiner();
        }
    }
    Matrix3d I = geo_.I_; // increase readability
    assert(I.isDiagonal()); // following Code only works for isometric cross sec
    // define matrices for material law, Factor 2.9 from ICRA Paper for elongation stiffness 
    double factor_ext = mat_.extensional_scaling;
    double factor_bend = mat_.bending_scaling;

    dep_.Kse.diagonal() << geo_.A_ * mat_.G_ , geo_.A_ * mat_.G_ , factor_ext*geo_.A_ * mat_.E_;
    dep_.Kse_e3 = Vector3d(0, 0, factor_ext*mat_.E_ * geo_.A_);
    dep_.Kbt.diagonal() << factor_bend*mat_.E_ * I.diagonal()[0], factor_bend*mat_.E_*I.diagonal()[1], mat_.G_*I.diagonal()[2];

    // compute compliance for ode
    dep_.Kse_inv = dep_.Kse.inverse();
    dep_.Kbt_inv = dep_.Kbt.inverse();
    // pre compute specific weight and inertia per unit length of robot
    dep_.rhoJ.diagonal() = mat_.rho_ * I.diagonal();
    dep_.rhoA = mat_.rho_ * geo_.A_;
    dep_.rhoAg = mat_.rho_ * geo_.A_ * dep_.g;
    // assemble damping matrices
    dep_.Bse = DiagonalMatrix<double, 3>(mat_.Bs_, mat_.Bs_, mat_.Be_);
    dep_.Bbt = DiagonalMatrix<double, 3>(mat_.Bb_, mat_.Bb_, mat_.Bt_);
    
    // compute inv. Kelvin-Voigt material model for pde (dynamics)
    dep_.inv_of_Kse_c0Bse = DiagonalMatrix<double, 3>(1 / (mat_.G_ * geo_.A_ + dep_.c0_ * mat_.Bs_),
                                                      1 / (mat_.G_ * geo_.A_ + dep_.c0_ * mat_.Bs_),
                                                      1 / (factor_ext*mat_.E_ * geo_.A_ + dep_.c0_ * mat_.Be_));

    dep_.inv_of_Kbt_c0Bbt = DiagonalMatrix<double, 3>(1 / (factor_bend*mat_.E_ * I.diagonal()[0] + dep_.c0_ * mat_.Bb_),
                                                      1 / (factor_bend*mat_.E_ * I.diagonal()[1] + dep_.c0_ * mat_.Bb_),
                                                      1 / (mat_.G_ * I.diagonal()[2] + dep_.c0_ * mat_.Bt_));
}

RodParameter::dep RodElement::getParam()
{
    return dep_;
}

RodParameter::mat& RodElement::setMaterial()
{
    return mat_;
}

int RodElement::getResolution(){
    return nNodes_;
}
void RodElement::display(){
    /* This function serves mostly debugging purpose and changes every now and 
    then. Might be outdated at some point, but it is currently not worth updating
    it to every change in the classes structure.*/
    std::cout << "--- RodElement --- " << std::endl;
    std::cout << "- Parameters:\n" << std::endl;
    if(chamb_.size()>0)
    {
        int index = 0;
        for(auto c: chamb_){
            std::cout << "AirChamber" << index << std::endl;
            c.display();
        }
    }
    std::cout << dep_ << std::endl;
}

