// This source file implements the dummy used to generate the S-Function
#ifdef DUMMY
#include <sl_func.h>
#include <iostream>
#include <unistd.h>

void SL_start_func(){
    std::cout<<"Using Cmake generated dummy function!"<<std::endl;
}

void SL_io_func(IN_type* ext_sim_in, PARAM_type* param, OUT_type* ext_sim_out){

usleep(1000); // 1000 ns pause for soft real time
}

void SL_terminate_func(){
}

void initSim(PARAM_type *ext_sim_in){

}
#endif


