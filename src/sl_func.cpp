// This implements the shared library used for simulations
#include <sl_function/sl_func.h>
#include <iostream>
#include <vector>
#include <unistd.h>
#include <custom_math/custom_math.h>
#include <sim_lib/manipulator.h>
#include <sim_lib/objective_function.h>

using namespace CRL;

Manipulator *SMR;
int iMat;            // indexer for copy process of material coordinate
int counter;
bool initParameters; // in first evaluation of sl io parameters may be writen
int step;
Eigen::VectorXd sim_in;
Eigen::VectorXd sim_out;
Vector6d reaction_wrench;

void SL_start_func()
{
    std::cout << "Using Cmake generated shared library!" << std::endl;
    std::cout << "Assembling continuum robot!" << std::endl;
    SMR = new Manipulator();
    initParameters = false;
    step = 0;
    // "assemble" manipulator

    // placement of airchambers as in Bartholdt et. al., 2021
    double rad_ch = 0.00515, dist_ch = 0.012;                                                        // in m
    double pos_ch_1[2] = {cos(180.0 / 180.0 * M_PI) * dist_ch, sin(180.0 / 180.0 * M_PI) * dist_ch}; // xy pos in m
    double pos_ch_2[2] = {cos(60.0 / 180.0 * M_PI) * dist_ch, sin(60.0 / 180.0 * M_PI) * dist_ch};   // xy pos in m
    double pos_ch_3[2] = {cos(300.0 / 180.0 * M_PI) * dist_ch, sin(300.0 / 180.0 * M_PI) * dist_ch}; // xy pos in m

    AirChamber chamb1(rad_ch, pos_ch_1);
    AirChamber chamb2(rad_ch, pos_ch_2);
    AirChamber chamb3(rad_ch, pos_ch_3);

    std::vector<AirChamber> chambers;
    chambers.push_back(chamb1);
    chambers.push_back(chamb2);
    chambers.push_back(chamb3);

    // define sections cap and actuator according to
    // Bartholdt et. al., 2021
    double rad = 0.021, rad_markerplate = 0.04, l_tube = 0.11, l_off = 0.010, l_mass = 0.00001;
    int nNodes_cap = 10, nNodes_actuator = 58, nNodes_mass = 2;

    RodElement cap(l_off, rad, nNodes_cap, false);
    RodElement actuator(l_tube, rad, nNodes_actuator, true, chambers);
    RodElement EE_mass(l_mass, rad, nNodes_mass, false);

    cap.setMaterial().rho_ = 1080;
    cap.setMaterial().E_ = 320000;
    cap.setMaterial().G_ = 1.0667e5;
    cap.setMaterial().Be_ = 0 * 0.01;
    cap.setMaterial().Bs_ = 0 * 0.01;
    cap.setMaterial().Bt_ = 0 * 0.01;
    cap.setMaterial().Bb_ = 0 * 0.01;
    cap.initDepParam();

    actuator.setMaterial().rho_ = 1070;
    actuator.setMaterial().Be_ = 0.0 * 0.01;
    actuator.setMaterial().Bs_ = 0.0 * 0.01;
    actuator.setMaterial().Bt_ = 0.0 * 0.01;
    actuator.setMaterial().Bb_ = 0.0 * 0.01;
    actuator.initDepParam();

    EE_mass.setMaterial().rho_ = 0.060 / (pow(rad, 2) * M_PI * l_mass); // total mass of 80g
    EE_mass.initDepParam();

    SMR->addRodElement(cap);
    SMR->addRodElement(actuator);
    SMR->addRodElement(cap);
    SMR->addRodElement(EE_mass);
    SMR->display();

    reaction_wrench = VectorXd::Zero(6);

    SMR->printState();
    std::cout << "Start dynamic sim!" << std::endl;
}

/*! As input of the beam external loads and pressure are foreseenq. Output is
the full state of the system.*/
void SL_io_func(IN_type *ext_sim_in, PARAM_type *param, OUT_type *ext_sim_out)
{
    step++;
    if (step == 1)
    {
        checkInputParameter(param);
    }

    // set parameters
    if (!initParameters && !(param == nullptr))
    {
        // set parameters of rod externally from Simulink
        initSim(param);
        initParameters = true;
    }

    if (step > 0 && !(param == nullptr))
    {
        setHyperelasticFix(param);
    }

    // Advance in time
    if (!(ext_sim_in == nullptr))
    {
        SMR->updateTime();
    }
    // Solve dynamic problem

    // set actuation pressure
    if (!(ext_sim_in == nullptr))
    {
        SMR->RodElements()[1].chamb_[0].setPressure(ext_sim_in->pressure[0]);
        SMR->RodElements()[1].chamb_[1].setPressure(ext_sim_in->pressure[1]);
        SMR->RodElements()[1].chamb_[2].setPressure(ext_sim_in->pressure[2]);
        
        SMR->RodElements()[1].chamb_[0].display();
        SMR->RodElements()[1].chamb_[1].display();
        SMR->RodElements()[1].chamb_[2].display();
    }
    if (!(ext_sim_in == nullptr))
    {
        SMR->setEEForceExternal(ext_sim_in->EEForceExternal);
        SMR->setEEMomentExternal(ext_sim_in->EEMomentExternal);
    }

    if (!(ext_sim_in == nullptr))
    {
        if (param->isDynamic && step > 1)
        {
            reaction_wrench = solveLevenbergMarquardt<objFunc<true>>(reaction_wrench, *SMR, 1e-10, 500);
        }
        else
        {
            reaction_wrench = solveLevenbergMarquardt<objFunc<false>>(reaction_wrench, *SMR, 1e-10, 500);
            // first step is solved statically to setup initial states
            if (param->isDynamic)
            {
                SMR->initTimeManager();
                std::cout << "Statics initialized\t wrench@" << reaction_wrench.transpose() << std::endl;
            }
        }
    }

    // process output
    if (!(ext_sim_out == nullptr))
    {
        Eigen::MatrixXd::Map(ext_sim_out->EE, 3, 1) = SMR->EE(); // set output
        // position of each node (Configuration)
        Eigen::MatrixXd::Map(ext_sim_out->rx, 1, TOTAL_RES) = SMR->state().block<1, TOTAL_RES>(INDEX_RX, 0);
        Eigen::MatrixXd::Map(ext_sim_out->ry, 1, TOTAL_RES) = SMR->state().block<1, TOTAL_RES>(INDEX_RY, 0);
        Eigen::MatrixXd::Map(ext_sim_out->rz, 1, TOTAL_RES) = SMR->state().block<1, TOTAL_RES>(INDEX_RZ, 0);
        // force of each node (Force)
        Eigen::MatrixXd::Map(ext_sim_out->nx, 1, TOTAL_RES) = SMR->state().block<1, TOTAL_RES>(INDEX_NX, 0);
        Eigen::MatrixXd::Map(ext_sim_out->ny, 1, TOTAL_RES) = SMR->state().block<1, TOTAL_RES>(INDEX_NY, 0);
        Eigen::MatrixXd::Map(ext_sim_out->nz, 1, TOTAL_RES) = SMR->state().block<1, TOTAL_RES>(INDEX_NZ, 0);
        // moments of each node (Moments)
        Eigen::MatrixXd::Map(ext_sim_out->mx, 1, TOTAL_RES) = SMR->state().block<1, TOTAL_RES>(INDEX_MX, 0);
        Eigen::MatrixXd::Map(ext_sim_out->my, 1, TOTAL_RES) = SMR->state().block<1, TOTAL_RES>(INDEX_MY, 0);
        Eigen::MatrixXd::Map(ext_sim_out->mz, 1, TOTAL_RES) = SMR->state().block<1, TOTAL_RES>(INDEX_MZ, 0);
        // orientation as quaternion
        Eigen::MatrixXd::Map(ext_sim_out->hi, 1, TOTAL_RES) = SMR->state().block<1, TOTAL_RES>(INDEX_HI, 0);
        Eigen::MatrixXd::Map(ext_sim_out->hj, 1, TOTAL_RES) = SMR->state().block<1, TOTAL_RES>(INDEX_HJ, 0);
        Eigen::MatrixXd::Map(ext_sim_out->hk, 1, TOTAL_RES) = SMR->state().block<1, TOTAL_RES>(INDEX_HK, 0);
        Eigen::MatrixXd::Map(ext_sim_out->h, 1, TOTAL_RES) = SMR->state().block<1, TOTAL_RES>(INDEX_H, 0);
        // translational velocity of each node (Moments)
        Eigen::MatrixXd::Map(ext_sim_out->qx, 1, TOTAL_RES) = SMR->state().block<1, TOTAL_RES>(INDEX_QX, 0);
        Eigen::MatrixXd::Map(ext_sim_out->qy, 1, TOTAL_RES) = SMR->state().block<1, TOTAL_RES>(INDEX_QY, 0);
        Eigen::MatrixXd::Map(ext_sim_out->qz, 1, TOTAL_RES) = SMR->state().block<1, TOTAL_RES>(INDEX_QZ, 0);
        // angular velocity of each node (Moments)
        Eigen::MatrixXd::Map(ext_sim_out->wx, 1, TOTAL_RES) = SMR->state().block<1, TOTAL_RES>(INDEX_WX, 0);
        Eigen::MatrixXd::Map(ext_sim_out->wy, 1, TOTAL_RES) = SMR->state().block<1, TOTAL_RES>(INDEX_WY, 0);
        Eigen::MatrixXd::Map(ext_sim_out->wz, 1, TOTAL_RES) = SMR->state().block<1, TOTAL_RES>(INDEX_WZ, 0);
        // copy material coordinates to output
        iMat = 0;
        for (auto s : SMR->s)
        {
            ext_sim_out->s[iMat] = s;
            iMat++;
        }
        // output chamber volumes into simulink
        counter = 0;
        for(auto rod: SMR->getRodElements())
        {
            if(rod.isActuated())
            {
                for(auto chamb: rod.chamb_)
                {
                    ext_sim_out->chamber_volume[counter] = chamb.getVolume();
                    counter++;
                }
            }
        }
    }
}

void SL_terminate_func()
{
    std::cout << "Ending node" << std::endl;
    std::cout << "Final state:" << std::endl;
    SMR->printState();
    delete SMR;
}

void initSim(PARAM_type *param)
{
    for (RodElement &rod : SMR->RodElements())
    {

        if (rod.isActuated())
        {
            rod.setMaterial().Bs_ = param->damping_coeffs[0];
            rod.setMaterial().Be_ = param->damping_coeffs[1];
            rod.setMaterial().Bb_ = param->damping_coeffs[2];
            rod.setMaterial().Bt_ = param->damping_coeffs[3];
            rod.setMaterial().extensional_scaling = param->stiffness_scaling[0];
            rod.setMaterial().bending_scaling = param->stiffness_scaling[1];
            rod.initDepParam();
        }
    }
    std::cout << "Scaling by" << param->stiffness_scaling[0] << "\t" << param->stiffness_scaling[1] << std::endl;
    std::cout << "SMR after re-intialisation: " << std::endl;
    SMR->display();
}

void setHyperelasticFix(PARAM_type *param)
{
    for (RodElement &rod : SMR->RodElements())
    {
        if (param->isHyperelastic && rod.isActuated())
        {
            std::cout << "Set actuated rod to be hyperelastic!" << std::endl;
            rod.setHyperelastic(true);
        }
    }
}

void checkInputParameter(PARAM_type *param)
{
    std::cout << "\tChecking SMR parameters" << std::endl;

    try
    {
        for (auto p : param->damping_coeffs)
            std::cout << "Damping\t" << p << std::endl;
        for (auto p : param->stiffness_scaling)
            std::cout << "Stiffness\t" << p << std::endl;
    }
    catch (std::exception e)
    {
        std::cout << e.what() << std::endl;
        throw(e);
    }

    for (auto s : param->stiffness_scaling)
    {
        assert(s>0);
    }

    for (auto &d : param->damping_coeffs)
    {
        try
        {
            assert(d>0); // damping has to greater than 0
        }
        catch(std::exception e)
        {
            std::cout << e.what() << std::endl;
            throw(e);
        }
    }
}