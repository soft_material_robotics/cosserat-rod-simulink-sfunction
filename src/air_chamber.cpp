#include <sim_lib/air_chamber.h>

AirChamber::AirChamber() {}

AirChamber::AirChamber(double radius, double posInCrossSec[2])
{
    areaCrossSec_ = M_PI * pow(radius, 2);
    posInCrossSec_ << posInCrossSec[0], posInCrossSec[1], 0;
    relPressure_  = 0;
    volume_ = 0;
}
AirChamber::~AirChamber() {}

AirChamber &AirChamber::operator=(const AirChamber &asignChamb)
{
    // Guard self assignment
    if(this == &asignChamb) return *this;
    areaCrossSec_ = asignChamb.areaCrossSec_;
    posInCrossSec_ = asignChamb.posInCrossSec_;
    return *this;
}

void AirChamber::setPressure(double &val)
{
    relPressure_ = val;
}

void AirChamber::setVolume(double val)
{
    volume_ = val;
}

void AirChamber::addPartialVolume(Vector3d vi, Vector3d ui, double ds)
{
    volume_ += (vi+ui.cross(posInCrossSec_)).norm()*areaCrossSec_*ds;
}

double AirChamber::getVolume()
{
    return volume_;
}

Vector3d AirChamber::getForce()
{
    // std::cout<<"Output Wrench from Chamber."<<std::endl;
    return relPressure_ * areaCrossSec_ * Vector3d::UnitZ();
}

Vector3d AirChamber::getLever()
{
    return posInCrossSec_;
}

double AirChamber::getArea()
{
    return areaCrossSec_;
}

Matrix3d AirChamber::getSteiner()
{
    // radius to center of area squared as vector
    Matrix3d r = Matrix3d::Zero();

    // Ixx = -A*y²
    r.diagonal()[0] = -areaCrossSec_*pow(posInCrossSec_[1],2);
    // Iyy = -A*x²
    r.diagonal()[1] = -areaCrossSec_*pow(posInCrossSec_[0],2);
    // steiner Ixx Iyy
    // polar (Izz)
    r.diagonal()[2] = r.diagonal()[0] + r.diagonal()[1];
    return r;
}

void AirChamber::display(){
std::cout<< "--- AirChamber ---"<<std::endl;
std::cout<< "Position in cross-section: \t" << posInCrossSec_.transpose() <<" m"<< std::endl;
std::cout<< "Area of cross-section: \t" << areaCrossSec_ << " qm"<< std::endl;
std::cout<< "Current Volume: \t" << volume_ <<" cubic meter" <<  std::endl;
} 