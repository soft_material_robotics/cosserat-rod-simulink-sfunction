#include <sim_lib/manipulator.h>

Manipulator::Manipulator()
{

}

Manipulator::~Manipulator()
{
}

void Manipulator::addRodElement(RodElement rod)
{
    std::cout<<"Old number of rod elements:\t"<<rod_.size();
    rod_.push_back(rod);
    std::cout<<"\t new number of rod elements:\t"<<rod_.size() << std::endl;
    /* set entries for material coordinate
    */
    double ds = rod.getRefLength() / (rod.getResolution() - 1);
    // init
    if (s.empty())
    {
        // first node, if not existent
        s.push_back(0);
        Y_.push_back(MatrixXd::Zero(nStates, 1));
        Z_.push_back(MatrixXd::Zero(nDeriv, 1));
        Z_h_.push_back(MatrixXd::Zero(nDeriv, 1));
    }
    else
    {
        // remove previously appended tip state
        s.pop_back();
        Y_.pop_back();
        Z_.pop_back();
        Z_h_.pop_back();
    }

    int prev_size = s.size();
    double last = s.back();

    // s_last_rod(end) = s_next_rod(0)
    for (double i = 0; i < rod.getResolution(); i++)
    {
        s.push_back(ds * i + last);
    }
    int current_size = s.size();

    // create state_matrix of size(nStates x rod.getResolution()) for every new segment
    Y_.push_back(MatrixXd::Zero(nStates, current_size - prev_size));
    Z_.push_back(MatrixXd::Zero(nDeriv, current_size - prev_size));
    Z_h_.push_back(MatrixXd::Zero(nDeriv, current_size - prev_size));

    // append doubled last node to account e.g. for discrete contact at tip
    s.push_back(s.back());
    Y_.push_back(MatrixXd::Zero(nStates, 1));
    Z_.push_back(MatrixXd::Zero(nDeriv, 1));
    Z_h_.push_back(MatrixXd::Zero(nDeriv, 1));
}

void Manipulator::initTimeManager()
{
    int iRod = 0;
    for (auto &Z : Z_)
    {
        time_scheme_.push_back(new TimeManagerBdfAlpha(Z, Z_h_[iRod], dt_, alpha_));
        iRod++;
    }
    updateThermoDynamics(); // after v and u are computed for the first time
}

void Manipulator::updateTime()
{
    for (auto &timeBDF : time_scheme_)
    {
        timeBDF->advanceTime();
    }
}

void Manipulator::updateThermoDynamics()
{
    // this has to start at 1 since Z_[0] is the first appended node 
    // (see class Manipulator)
    int iRod = 1; 
    for(auto &rod: rod_)
    {
        if(rod.isActuated())
        {
            thermodyn_.computeVolume(rod, Z_[iRod]);
        }
        iRod++;
    }
}

std::vector<RodElement> Manipulator::getRodElements()
{
    return rod_;
}
std::vector<RodElement> &Manipulator::RodElements()
{
    return rod_;
}

int Manipulator::getSpatialRes()
{
    // not required anymore
    return 0;
}




// functions to return state and history terms

MatrixXd Manipulator::state()
{
    MatrixXd out;
    const int rows = nStates;
    int cols = 0;
    for (auto &state : Y_)
    {
        cols += state.cols();
    }
    assert(cols != 0);
    out = MatrixXd::Zero(rows, cols);
    int iCol = 0;
    for (auto &state : Y_)
    {
        for (int i = 0; i < state.cols(); i++)
        {
            assert(out.rows() == state.rows());
            out.block<rows, 1>(0, iCol) = state.col(i);
            iCol++;
        }
    }
    return out;
}

MatrixXd Manipulator::laststate()
{

    MatrixXd out;
    // determin size of out
    const int rows = nDeriv;
    int cols = 0;
    for (auto &laststate : Z_)
    {
        cols += laststate.cols();
    }
    assert(cols != 0);
    out = MatrixXd::Zero(rows, cols);
    int iCol = 0;
    // fill output columnwise
    for (auto &laststate : Z_)
    {
        for (int i = 0; i < laststate.cols(); i++)
        {
            assert(out.rows() == laststate.rows());
            out.block<rows, 1>(0, iCol) = laststate.col(i);
            iCol++;
        }
    }
    return out;
}

MatrixXd Manipulator::history()
{
    MatrixXd out;
    const int rows = nDeriv;
    int cols = 0;
    for (auto &hist : Z_h_)
    {
        cols += hist.cols();
    }
    assert(cols != 0);
    out = MatrixXd::Zero(rows, cols);
    int iCol = 0;
    for (auto &hist : Z_h_)
    {
        for (int i = 0; i < hist.cols(); i++)
        {
            assert(out.rows() == hist.rows());
            out.block<rows, 1>(0, iCol) = hist.col(i);
            iCol++;
        }
    }
    return out;
}


Vector3d Manipulator::EE(){
    return Y_.back().block<3,1>(0,0);
}

void Manipulator::setEEForceExternal(double forceExternal[3]){
    std::vector<double> a = {forceExternal[0], forceExternal[1], forceExternal[2]};
    EEForceExternal = Eigen::Map<Eigen::Vector3d>(a.data(),a.size());
    std::cout << "EE Force External: " << '\n' << EEForceExternal <<'\n';
}

void Manipulator::setEEMomentExternal(double momentExternal[3]){
    std::vector<double> a = {momentExternal[0], momentExternal[1], momentExternal[2]};
    EEMomentExternal = Eigen::Map<Eigen::Vector3d>(a.data(),a.size());
    std::cout << "EE Moment External: "<< '\n' << EEMomentExternal <<'\n';

}

// These functions are primarily required for debugging and standalone apps.
void Manipulator::printState()
{
    std::cout << "Current state: \n\n";
    // int nRows = state().rows();
    // for (int iRows = 0; iRows < nRows; iRows++)
    // {
    //     std::cout << state().row(iRows) << "\n\n";
    // }
    std::cout << state().transpose() << "\n\n";
}

void Manipulator::printLastState(){
        std::cout << "Last state Z = q w v u: \n\n";
    int nRows = laststate().rows();
    for (int iRows = 0; iRows < nRows; iRows++)
    {
        std::cout << laststate().row(iRows) << "\n\n";
    }
}

void Manipulator::printHistTerms()
{
    std::cout << "Current history terms Z_H: \n\n";
    int nRows = history().rows();
    for (int iRows = 0; iRows < nRows; iRows++)
    {
        std::cout << history().row(iRows) << "\n\n";
    }
}

void Manipulator::display(){
    for(auto &r: rod_){
        r.display();
    }
}