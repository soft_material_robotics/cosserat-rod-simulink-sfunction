#include <sim_lib/thermodynamics.h>

ThermoDynamics::ThermoDynamics()
{
    vi_ = Vector3d::UnitZ();
    ui_ = Vector3d::Zero();
}

ThermoDynamics::~ThermoDynamics() {}

void ThermoDynamics::computeVolume(RodElement &rod, MatrixXd &Z)
{
    for (auto &chamb : rod.chamb_)
    {
        chamb.setVolume(); //!< sets volume to zero (required)
    }
    ds_ = rod.getRefLength() / (double)(rod.getResolution() - 1); // get integration increment
    for (int i = 0; i < rod.getResolution() - 1; i++)
    {
        // debug here
        // std::cout << "i: \t" << i << std::endl;  
        vi_ << Z.col(i).segment(6, 3);
        ui_ << Z.col(i).segment(9, 3);
        // std::cout << "v\t" << vi_.transpose() << std::endl;
        // std::cout << "u\t" << ui_.transpose() << std::endl;
        for (auto &chamb : rod.chamb_)
        {

            chamb.addPartialVolume(vi_, ui_, ds_);
        }
    }
}
