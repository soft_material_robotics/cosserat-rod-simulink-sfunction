#include <test_lib/test_ode.h>

TestODE::TestODE(){

}
TestODE::~TestODE(){

}

void TestODE::step(VectorXd &y_s_out, Map<VectorXd> z_out, Map<VectorXd> y, RodElement &rod){

// dx = A x 
Map<VectorXd>dx(&y_s_out[0],2);
Map<VectorXd>x(&y(0),2);
Matrix2d A = Matrix2d::Zero();
A << 0, 1, -1, 0; // 1d oszillator with w = sqrt(k/m) = 1, no damping
dx = A*x;
std::cout << "x"<< x.transpose() << std::endl;
std::cout << "dx"<< dx.transpose() << std::endl;

}
