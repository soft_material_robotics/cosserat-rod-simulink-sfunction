#define CATCH_CONFIG_MAIN
#define NEARZERO 1e-10
// external
#include <catch2/catch.hpp>
#include <Eigen/Dense>

// continuum robot lib (John Till)
#include <custom_math/custom_math.h>

// code base
#include <sim_lib/rod_element.h>
#include <sim_lib/air_chamber.h>
#include <sim_lib/cosserat_rod_quat.h>

#include <sim_lib/manipulator.h>
#include <sl_function/sl_func.h>
#include <sim_lib/objective_function.h>

// test libs
#include <test_lib/test_ode.h>

// std libs
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <cmath>

TEST_CASE("Test Map class looping rowwise through matrix", "[Eigen]")
{

    const int nrow = 5;
    const int ncol = 10;
    // add 20 MatrixXd Object of growing size to container
    std::cout << "\n\n---Test Eigen::Map<VectorXd> with Eigen MatrixXd class.\n";
    std::cout << "-Create container with zero matrices if different size\n";

    MatrixXd mat = MatrixXd::Zero(nrow, ncol);
    MatrixXd mat_exp = MatrixXd::Zero(nrow, ncol);
    std::cout << "-Manipulate entries with Eigen::Map class\n";
    for (int iCols = 0; iCols < mat.cols(); iCols++)
    {
        Map<VectorXd> vec(&mat(0, iCols), nrow);
        std::cout << vec.transpose() << " + " << (float)iCols * VectorXd::Ones(nrow).transpose() << std::endl;
        vec = vec + (float)iCols * VectorXd::Ones(nrow);
        std::cout << " =  " << vec.transpose() << std::endl;
    }
    std::cout << "\n\nResulting Matrix" << std::endl;
    std::cout << mat << std::endl
              << std::endl;

    REQUIRE(true);
}

TEST_CASE("Test container class + eigen", "[Eigen]")
{
    std::vector<MatrixXd> container;
    const int nrow = 10;
    // add 20 MatrixXd Object of growing size to container
    std::cout << "\n\n---Test STL container with Eigen MatrixXd class.\n";
    std::cout << "-Create container with zero matrices if different size\n";
    for (int i = 0; i < 10; i++)
    {
        container.push_back(MatrixXd::Zero(nrow, i));
    }

    std::cout << "-Manipulate entries with Eigen::Map class\n";
    for (auto &mat : container)
    {
        std::cout << "nCols = " << mat.cols() << std::endl;
        for (int iCols = 0; iCols < mat.cols(); iCols++)
        {

            Map<VectorXd> vec(&mat(0, iCols), nrow);
            std::cout << vec.transpose() << " + " << (float)iCols * VectorXd::Ones(nrow).transpose() << std::endl;
            vec = vec + (float)iCols * VectorXd::Ones(nrow);
            std::cout << " =  " << vec.transpose() << std::endl;
        }
        std::cout << "\n\nResulting Matrix" << std::endl;
        std::cout << mat << std::endl
                  << std::endl;
    }

    // VectorXd result = container.back().row(0);
    // VectorXd exp = VectorXd(result.cols());
    // for(float i = 0; i<result.cols(); i++)
    // {
    //     exp[i] = i; 
    // }
    // REQUIRE(exp == result);
}

TEST_CASE("Test runge kutta 4th order with test ode", "[rk4]"){
    /* length of rod and number nodes determines resolution of integration */
    double sim_time  = 2*M_PI; 
    int nTimeSteps = 40;
    double dummy_val = 0;
    /* we want to test the actual implementation for the rod integration with 
    a different ODE with known solution (see test_ode)*/
    RodElement dummy_rod(sim_time, dummy_val,nTimeSteps,false);
    //init numeric integrations
    VectorXd init_values;
    init_values = VectorXd::Zero(nStates);
    init_values[0] = 1; // position in m
    init_values[1] = 0; // velocity in m/s

    MatrixXd Y(nStates,nTimeSteps);
    Y.setZero();
    rungekutta4<TestODE::step, nStates, nDeriv>(Y, Y, init_values, dummy_rod.getRefLength(), dummy_rod);
    VectorXd x_exp = VectorXd::Zero(nTimeSteps);
    double dt = sim_time/(double)(nTimeSteps-1);

    // this is the exact solution to the formulated problem ddx + x = 0
    // with initial conditon x = 1, dx = 0;
    std::cout << "--Analytical vs. Solver--"<< std::endl<< std::endl;
    std::cout << "Step \t|Analytical\t|Solver\t|Error"<< std::endl;
    VectorXd x_solver = Y.row(0);
    VectorXd x_err = VectorXd::Zero(nTimeSteps);
    for(double istep = 0.0; istep < (double)nTimeSteps-1; istep++)
    {
    
        x_exp[istep] = cos(dt*istep); //analytical solution
        x_err[istep] = x_exp[istep]-x_solver[istep];

        std::cout << istep << "\t|" << x_exp[istep]
        <<"\t|" << x_solver[istep]
        <<"\t|"<< x_exp[istep]-x_solver[istep]<< std::endl;
    }
    REQUIRE(x_err.norm() < 1e-3); // empirical value for tolerance
}

// TEST_CASE("Test correction of history terms", "[rk4/ode]")
// {

// // calculate angular velocity with rotation matrices
// Vector3d omega = Vector3d::Uni
// Matrix3d R1 = Matrix3d::Identity();
// Matrix3d R2 = 


// // correctHistoryTermsRungeKutta4();

// }

TEST_CASE(" ","[]")
{
    Vector3d vec;
    MatrixXd mat = MatrixXd::Zero(2,12);
    vec = mat.block<1,3>(0,0);
    std::cout << vec;

}
