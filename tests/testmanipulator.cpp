#define CATCH_CONFIG_MAIN
#define NEARZERO 1e-10
// external
#include <catch2/catch.hpp>
#include <Eigen/Dense>

// continuum robot lib (John Till)
#include <custom_math/custom_math.h>

// code base
#include <sim_lib/rod_element.h>
#include <sim_lib/air_chamber.h>
#include <sim_lib/cosserat_rod_quat.h>

#include <sim_lib/manipulator.h>
#include <sl_function/sl_func.h>
#include <sim_lib/objective_function.h>

// std libs
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <cmath>

/*  */
TEST_CASE("Create rod element without airchambers", "[RodElement]")
{
    // init test parameters
    double rad = 0.6;
    double length = 12;
    int nNodes = 10;
    bool init = false;

    // attempt to initialize the rod element without airchambers
    try
    {
        RodElement test_rod(length, rad, nNodes, false);
        init = true;
    }
    catch (const std::exception &e) // reference to the base of a polymorphic object
    {
        init = false;
        std::cout << e.what();
    }
    // initialisation needs to work
    REQUIRE(init);
}

/* Check if constructor RodElement initialises parameters. */
TEST_CASE("Check if parameters are intialized correctly", "[RodElement]")
{
    // init test parameters
    double rad = 0.6;
    double length = 12;
    int nNodes = 2;
    bool init = false;
    double radChamb = 0.001;          // in m
    double posChamb[2] = {0.1, -0.1}; // in m
    double pressure = 10e5;           // in Pa
    RodElement test_rod2(length, rad, nNodes, false);
    AirChamber test_chamb(radChamb, posChamb);
    std::vector<AirChamber> chambers;
    chambers.push_back(test_chamb);
    RodElement test_rod3(length, rad, nNodes, true, chambers);

    // assert
    REQUIRE((test_rod2.getParam().c0_ != 0 &&
             test_rod3.getParam().c0_ != 0));
}

TEST_CASE("Check Area of momentum for RodElement", "[RodElement]"){
    // init rod with arbitrary parameters
    double radRod = 1.0;
    double lengthRod = 1.0;
    int nNodes = 2;
    RodElement test_rod(radRod, lengthRod, nNodes, false);

    test_rod.setMaterial().E_ = 1.0; // if E/G-Mod is equal 1, K_se/bt becomes A/I
    test_rod.setMaterial().G_ = 1.0; // 
    test_rod.initDepParam();
    Matrix3d I = test_rod.getParam().Kbt;
    Vector3d diag_I_exp = Vector3d::Zero();
    // calculation of area of momentum for circular cross sec.
    diag_I_exp << M_PI*pow(radRod,4)/4, M_PI*pow(radRod,4)/4, M_PI*pow(radRod,4)/2;

    REQUIRE(diag_I_exp==I.diagonal());
  
}

TEST_CASE("Check Steiner from AirChambers", "[AirChamber]")
{
    // deviation moments are expected to be zero
    // only symmetric air chambers currently possible

    double r = 1, posx = 0.1, posy = 0.1;
    double pos[2] = {posx, posy};
    AirChamber chamb(r, pos);
    Matrix3d steiner_exp = Matrix3d::Zero();
    steiner_exp.diagonal()[0] = -pow(posx, 2) * chamb.getArea();
    steiner_exp.diagonal()[1] = -pow(posy, 2) * chamb.getArea();
    steiner_exp.diagonal()[2] = (-pow(posy, 2) + -pow(posx, 2)) * chamb.getArea();
    Matrix3d steiner_is = chamb.getSteiner();

    REQUIRE(steiner_exp == steiner_is);
}

TEST_CASE("Check number of rod elements in empty manipulator", "[Manipulator]")
{
    Manipulator SMR;
    REQUIRE(SMR.getRodElements().size() == 0);
}

TEST_CASE("Replicating a bug where manipulator has more RodElements then intended", "[Manipulator]")
{
    double radRod = 1, lengthRod = 1;
    int nNodes = 2;
    Manipulator SMR;
    RodElement test_rod(radRod, lengthRod, nNodes, false);
    SMR.addRodElement(test_rod);
    REQUIRE(SMR.getRodElements().size() == 1);
}

/*Test rod ode (static system) with beam element*/
TEST_CASE("Test numerical integration (euler) with rod class", "[cosseratRodOde]")
{
    // init test parameters
    std::cout << " --- Init test\n\n";
    double radRod = 0.6;
    double lengthRod = 12;
    int nNodes = 2;
    RodElement test_rod(lengthRod, radRod, nNodes, false);
    static MatrixXd Y(19, 2);
    static MatrixXd Z(12, 2);
    VectorXd y0(19); // intial state value of ode
    y0.setZero();
    Vector3d pos0; // position
    pos0.setZero();
    Quaterniond h0; // cross sec orientation
    h0.setIdentity();
    Vector3d force; // force
    force.setZero();
    Vector3d moment; // moment
    moment.setZero();
    y0 << pos0,h0.coeffs(), force, moment, Vector6d::Zero(); // no velocities

    // define expected results
    Vector3d rExp = Vector3d::UnitZ() * lengthRod;
    Vector3d nExp = -test_rod.getParam().rhoAg * lengthRod;
    std::cout << " --- start euler\n\n";
    // evaluate ODE a single time
    euler<cosseratRodOde, 19, 12>(Y, Z, y0, lengthRod, test_rod);
    Vector3d r = Y.block<3, 1>(0, 1);
    Quaterniond h = Quaterniond(Y.block<4, 1>(3, 1));
    Vector3d n = Y.block<3, 1>(7, 1);
    Vector3d m = Y.block<3, 1>(10, 1);

    bool bPos = (r == rExp);
    bool bOri = (h == h0);
    bool bForce = (n == nExp);
    bool bMoment = (m == moment);

    REQUIRE((bPos && bOri && bForce && bMoment));
}

/*assemble manipulator from multiple rods and check material coordinate*/
TEST_CASE("Assemble Manipulator and validate material coordinate", "[Manipulator]")
{
    bool success = false;

    // init test parameters
    int nRods = 3;
    double radRod = 0.06;
    double lengthRod = 0.10;
    int nNodes = 50; // only 2 nodes are required to test the ode

    double radChamb = 0.01;          // in m
    double posChamb[2] = {0.0, 0.0}; // in m

    // implement a single airchamber at the center of the rod.
    AirChamber test_chamb(radChamb, posChamb);
    std::vector<AirChamber> chambers;
    chambers.push_back(test_chamb);

    // create rod
    RodElement test_rod(lengthRod, radRod, nNodes, true, chambers);

    // add multiple rods to manipulator
    Manipulator SMR = Manipulator();
    std::vector<double> s_exp;
    for (int i = 0; i < nRods; i++)
    {
        SMR.addRodElement(test_rod);
        if (s_exp.empty())
            s_exp.push_back(0);
        double last = s_exp.back();

        for (double i = 0; i < nNodes; i++)
        {
            s_exp.push_back(lengthRod / (nNodes - 1) * i + last);
        }
    }

    // compute expected material coordinate

    // assert
    std::cout << "Material coordinate:" << std::endl;

    for (auto s : SMR.s)
        std::cout << s << "\t";
    std::cout << std::endl
              << std::endl
              << std::endl;
    REQUIRE(SMR.s.back() - lengthRod * 3 < NEARZERO);
}

/*Solve ODE for manipulator. In this test case the manipulator is hanging
from the ceiling. It can be modeled with simple extensional rod under
gravitational load. Therefore, the solution of the boundary value problem is
n0 = [0 0 g*A*l]^T and m0 = [0, 0, 0]^T  */
TEST_CASE("The ode for serially connected rods is tested", "[]")
{
    bool success = false;

    // init test parameters
    int nRods = 3;
    double radRod = 0.06;
    double lengthRod = 0.10;
    int nNodes = 2;             // only 2 nodes are required to test the ode

    double radChamb = 0.01;          // in m
    double posChamb[2] = {0.0, 0.0}; // in m
    double pressure = 0;

    // implement a single airchamber at the center of the rod.
    AirChamber test_chamb(radChamb, posChamb);
    std::vector<AirChamber> chambers;
    chambers.push_back(test_chamb);

    Manipulator SMR = Manipulator();
    RodElement test_rod(lengthRod, radRod, nNodes, true, chambers);
    for (int i = 0; i < nRods; i++)
        SMR.addRodElement(test_rod);

    // configure y0
    Vector3d force;
    force.setZero(); // force

    // n0 = [0 0 g*A*l_total]^T
    force << 0, 0, test_rod.getParam().rhoAg.norm() * test_rod.getRefLength() * nRods;
    Vector3d moment;
    moment.setZero(); // moment

    // perform numerical integration (statics)
    Vector6d guess;
    guess << force, moment;
    Vector6d wrench = objFunc<false>(guess, SMR);
    // assert: if force is solution of ode n_tip needs to vanish
    REQUIRE(wrench.norm() < NEARZERO);
}

/* Use the levenberg-marquardt algorithm to solve static problem*/
TEST_CASE("Check Levenberg-Marquard algorithm", "[]")
{
    bool success = false;

    // init test parameters
    double radRod = 0.06;
    double lengthRod = 0.10;
    double radChamb = 0.01;          // in m
    int nNodes = 10;

    double posChamb[2] = {0.0, 0.0}; // in m
    const int nRods = 3;
    double pressure = 0;
    // implement a single airchamber at the center of the rod.
    AirChamber test_chamb(radChamb, posChamb);

    test_chamb.setPressure(pressure);
    std::vector<AirChamber> chambers;
    chambers.push_back(test_chamb);
    Manipulator SMR;
    RodElement test_rod(lengthRod, radRod, nNodes, true, chambers);
    for (int i = 0; i < nRods; i++)
        SMR.addRodElement(test_rod);

    Vector3d force;
    force.setZero(); // force
    // n0 = [0 0 g*A*l_total]^T
    force << 0, 0, test_rod.getParam().rhoAg.norm() * test_rod.getRefLength() * nRods;

    // perform optimization (statics)
    VectorXd guess = VectorXd::Zero(6, 1);
    VectorXd wrench = VectorXd::Zero(6, 1);
    // solve static bvp
    wrench = solveLevenbergMarquardt<objFunc<false>>(guess, SMR, 1e-20);

    // assert: if force is solution of ode n_tip needs to vanish
    REQUIRE(wrench.norm() - force.norm() < NEARZERO);
}

/* Check if initialisation of the member of type TimeManagerBdfAlpha works*/
TEST_CASE("Test timemanagement", "[TimeBdfAlpha]")
{
    std::cout<<"\n\n\n\t---Test Numerical BDF-alpha scheme\n\n";
    // init test parameters
    double radRod = 0.06;
    double lengthRod = 0.10;
    double radChamb = 0.01;          // in m
    int nNodes = 10;

    double posChamb[2] = {0.0, 0.0}; // in m
    const int nRods = 3;
    double pressure = 0;

    double dt = TIMESTEP; 
    double alpha = ALPHA;

    // implement a single airchamber at the center of the rod.
    AirChamber test_chamb(radChamb, posChamb);

    test_chamb.setPressure(pressure);
    std::vector<AirChamber> chambers;
    chambers.push_back(test_chamb);

    Manipulator SMR;
    RodElement test_rod(lengthRod, radRod, nNodes, true, chambers);
    for (int i = 0; i < nRods; i++)
        SMR.addRodElement(test_rod);

    Vector3d force;
    force.setZero(); // force
    // n0 = [0 0 g*A*l_total]^T
    force << 0, 0, test_rod.getParam().rhoAg.norm() * test_rod.getRefLength() * nRods;

    // perform optimization (statics)
    VectorXd guess = VectorXd::Zero(6, 1);
    VectorXd wrench = VectorXd::Zero(6, 1);
    // solve static bvp
    wrench = solveLevenbergMarquardt<objFunc<false>>(guess, SMR, 1e-20);
    SMR.printLastState();
    SMR.printHistTerms();
    SMR.initTimeManager();
    SMR.printLastState();
    SMR.printHistTerms();   
    
    // Z_h_ == (c1+c2)* Z
    double c1 = -2.0 / dt;
    double c2 = (0.5 + alpha) / (dt * (1 + alpha));
    std::cout << (c1 + c2) * SMR.laststate() - SMR.history() << std::endl;


    // bool historyCorrect = (c1 + c2) * SMR.laststate() == SMR.history();

    /* */
    bool historyCorrect = ((c1 + c2) * SMR.laststate() - SMR.history()).norm() < NEARZERO;
    
    REQUIRE(historyCorrect);
}

/* Use the timeBDFscheme to solve a dynamic initial-boundary value problem*/
TEST_CASE("Check TimeBDF algorithm", "[solveLevenbergMarquardt]")
{

    // init test parameters
    double radRod = 0.06;
    double lengthRod = 0.10;
    int nNodes = 4;

    double radChamb = 0.01;          // in m
    double posChamb[2] = {0.0, 0.0}; // in m
    const int nRods = 3;
    double pressure = 0;

    // implement a single airchamber at the center of the rod.
    AirChamber test_chamb(radChamb, posChamb);

    test_chamb.setPressure(pressure);
    std::vector<AirChamber> chambers;
    chambers.push_back(test_chamb);
    Manipulator SMR;
    RodElement rod(lengthRod, radRod, nNodes, false);
    rod.setMaterial().rho_ = 0;
    for (int i = 0; i < nRods; i++)
        SMR.addRodElement(rod);

    Vector3d force;
    force.setZero(); // force
    // n0 = [0 0 g*A*l_total]^T
    force << 0, 0, rod.getParam().rhoAg.norm() * rod.getRefLength() * nRods;

    // perform optimization (statics)
    VectorXd guess = VectorXd::Zero(6, 1);
    VectorXd wrench = VectorXd::Zero(6, 1);
    VectorXd initial_wrench = VectorXd::Zero(6, 1);
    // solve static bvp
    wrench = solveLevenbergMarquardt<objFunc<false>>(guess, SMR, 1e-20);
    SMR.initTimeManager();

    // SMR.printState();
    // SMR.printLastState();
    // SMR.printHistTerms();

    pressure = 0;
    initial_wrench = wrench;
    // SMR.RodElements()[0].chamb_[0].setPressure(pressure);
    for (int iStep = 0; iStep < 10; iStep++)
    {
        
        SMR.updateTime();
        // Solve dynamic problem
        wrench = solveLevenbergMarquardt<objFunc<true>>(wrench, SMR);
    }
    REQUIRE((initial_wrench - wrench).norm() < NEARZERO);
}

/* Use pressure to actuate single chamber actuator. This test only proofs, that
the acctuator accelerates.*/
TEST_CASE("Check actuation in dynamic simulation", "[solveLevenbergMarquardt]")
{

    // init test parameters
    double radRod = 0.06;
    double lengthRod = 0.10;
    int nNodes = 20;
    double radChamb = 0.01;          // in m
    double posChamb[2] = {0.0, 0.0}; // in m
    const int nRods = 3;
    double pressure = 0;

    // implement a single airchamber at the center of the rod.
    AirChamber test_chamb(radChamb, posChamb);

    test_chamb.setPressure(pressure);
    std::vector<AirChamber> chambers;
    chambers.push_back(test_chamb);
    
    Manipulator SMR;
    RodElement rod(lengthRod, radRod, nNodes, true, chambers);
    rod.setMaterial().rho_ = 0;
    for (int i = 0; i < nRods; i++)
        SMR.addRodElement(rod);

    // perform optimization (statics)
    VectorXd guess = VectorXd::Zero(6, 1);
    VectorXd wrench = VectorXd::Zero(6, 1);
    VectorXd initial_wrench = VectorXd::Zero(6, 1);
    // solve static bvp
    wrench = solveLevenbergMarquardt<objFunc<false>>(guess, SMR, 1e-20);
    SMR.initTimeManager();

    // SMR.printState();
    // SMR.printLastState();
    // SMR.printHistTerms();

    pressure = 10000;
    initial_wrench = wrench;
    SMR.RodElements()[0].chamb_[0].setPressure(pressure);
    for (int iStep = 0; iStep < 10; iStep++)
    {

        SMR.updateTime();
        // Solve dynamic problem
        wrench = solveLevenbergMarquardt<objFunc<true>>(wrench, SMR);
    }
    REQUIRE(!((initial_wrench - wrench).norm() < NEARZERO));
}

/*Test if hyperelasticity is called properly*/
TEST_CASE("Test if hyperelasitic material equation is called", "[cosseratRodOde]")
{
    // init test parameters
    std::cout << " --- Init test\n\n";
    double radRod = 0.6;
    double lengthRod = 12;
    int nNodes = 2;
    RodElement test_rod(lengthRod, radRod, nNodes, false);
    test_rod.setHyperelastic(true);
    static MatrixXd Y(19, 2);
    static MatrixXd Z(12, 2);
    VectorXd y0(19); // intial state value of ode
    y0.setZero();
    Vector3d pos0; // position
    pos0.setZero();
    Quaterniond h0; // cross sec orientation
    h0.setIdentity();
    Vector3d force; // force
    force.setZero();
    Vector3d moment; // moment
    moment.setZero();
    y0 << pos0,h0.coeffs(), force, moment, Vector6d::Zero(); // no velocities

    // evaluate ODE a single time
    euler<cosseratRodOde, 19, 12>(Y, Z, y0, lengthRod, test_rod);

    REQUIRE(true);
}

/*Test if visco-hyperelasticity is called properly*/
TEST_CASE("Test if visco-hyperelasticity material equation is called", "[cosseratRodOde]")
{
    // init test parameters
    std::cout << " --- Init test\n\n";
    double radRod = 0.025;
    double lengthRod = 0.13;
    int nNodes = 4;
    // init one hyperelastic rod
    RodElement hyperelastic_rod(lengthRod, radRod, nNodes, false);
    hyperelastic_rod.setHyperelastic(true);
    hyperelastic_rod.setMaterial().rho_ = 0;
    // these parameters are used to derive the visco-hyperelastic material
    hyperelastic_rod.setMaterial().Be_ = 0.4;
    hyperelastic_rod.setMaterial().Bs_ = 0.4;
    hyperelastic_rod.initDepParam();
    Manipulator SMR_hyper;
    SMR_hyper.addRodElement(hyperelastic_rod);

    // init one elastic rod
    RodElement linearelastic_rod(lengthRod, radRod, nNodes, false);
    linearelastic_rod.setHyperelastic(false);
    linearelastic_rod.setMaterial().rho_ = 0;
    hyperelastic_rod.setMaterial().Be_ = 0.4;
    hyperelastic_rod.setMaterial().Bs_ = 0.4;
    linearelastic_rod.initDepParam();
    Manipulator SMR_linear;
    SMR_linear.addRodElement(linearelastic_rod);

    // perform optimization (statics)
    VectorXd guess = VectorXd::Zero(6, 1);
    VectorXd wrench_hyp = VectorXd::Zero(6, 1);
    VectorXd wrench_lin = VectorXd::Zero(6, 1);
    VectorXd initial_wrench = VectorXd::Zero(6, 1);

    // solve static bvp
    wrench_hyp = solveLevenbergMarquardt<objFunc<false>>(guess, SMR_linear, 1e-20);
    wrench_lin = solveLevenbergMarquardt<objFunc<false>>(guess, SMR_hyper, 1e-20);

    // Debug
    std::cout<< "Difference of solution hyp - lin:\t"<<(wrench_hyp-wrench_lin).transpose()<<std::endl;
    SMR_linear.initTimeManager();
    SMR_hyper.initTimeManager();
    std::cout << "\n\n\n---Hyperelastic:"<<std::endl;
    SMR_hyper.printState();
    SMR_hyper.printLastState();
    SMR_hyper.printHistTerms();
    std::cout << "\n\n\n---Linearelastic:"<<std::endl;
    SMR_hyper.printState();
    SMR_linear.printLastState();
    SMR_linear.printHistTerms();
std::cout<< "------------Compute dynamics----------------"<<std::endl;
std::cout << "EE hyper vs EE linear"<< std::endl;
    for (int iStep = 0; iStep < 10; iStep++)
    {
        // Solve dynamic problem
        wrench_hyp = solveLevenbergMarquardt<objFunc<true>>(wrench_hyp, SMR_hyper);
        wrench_lin = solveLevenbergMarquardt<objFunc<true>>(wrench_lin, SMR_linear);
        SMR_hyper.updateTime();
        SMR_linear.updateTime();
        std::cout<< SMR_hyper.EE().transpose()<<"\t"<<SMR_linear.EE().transpose()<<std::endl;
    }
    REQUIRE(true);
}

TEST_CASE("Test external force", "[cosseratRodOde]")
{
    std::cout << " --- Init test\n\n";
    double radRod = 0.002;
    double lengthRod = 0.15;
    int nNodes = 50;
    RodElement test_rod(lengthRod, radRod, nNodes, false);
    test_rod.setHyperelastic(false);
    test_rod.setMaterial().E_ = 200e9;
    test_rod.setMaterial().G_ = 200e9/3;
    test_rod.initDepParam();
    Manipulator SMR;
    SMR.addRodElement(test_rod);
    double f_ext[3] = {1,0,0};
    SMR.setEEForceExternal(f_ext);
    VectorXd wrench = VectorXd::Zero(6,1);
    try{
        wrench = solveLevenbergMarquardt<objFunc<false>>(wrench, SMR, 1e-10,50);
    }
    catch(std::exception e)
    {
        std::cout << e.what() << std::endl;
    }

    //SMR.printState();
    REQUIRE(true);
}