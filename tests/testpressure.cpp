#define CATCH_CONFIG_MAIN
#define NEARZERO 1e-6
// external
#include <catch2/catch.hpp>
#include <Eigen/Dense>

// continuum robot lib (John Till)
#include <custom_math/custom_math.h>

// code base
#include <sim_lib/rod_element.h>
#include <sim_lib/air_chamber.h>
#include <sim_lib/cosserat_rod_quat.h>

#include <sim_lib/manipulator.h>
#include <sl_function/sl_func.h>
#include <sim_lib/objective_function.h>

// std libs
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <cmath>

/* check if the force returned from AirChamber is correct*/
TEST_CASE("Force Calculation Airchamber", "[AirChamber]")
{
    // init test parameters
    double radius = 1;       // in m
    double pos[2] = {1, -1}; // in m
    double pressure = 10e5;  // in Pa
    Eigen::Vector3d force;
    force.setZero();
    // calculate wrench
    force = pressure * M_PI * pow(radius, 2) * Vector3d::UnitZ();

    // use air chamber class to compute wrench
    AirChamber test_chamb(radius, pos);
    test_chamb.setPressure(pressure);

    // result must be the same
    REQUIRE(test_chamb.getForce() == force);
}

/* check if the moment returned from AirChamber is correct*/
TEST_CASE("Moment Calculation Airchamber", "[AirChamber]")
{
    // init test parameters
    double radius = 1;       // in m
    double x = 0.1, y = 0.1; //  in m
    double pos[2] = {x, y};
    double pressure = 10e5; // in Pa
    double moment;
    // calculate wrench
    moment = sqrt(pow(x, 2) + pow(y, 2)) * pressure * M_PI * pow(radius, 2);

    // use air chamber class to compute wrench
    AirChamber test_chamb(radius, pos);
    test_chamb.setPressure(pressure);

    // result must be the same
    REQUIRE(test_chamb.getLever().cross(test_chamb.getForce()).norm() - moment < NEARZERO);
}

/* check if the moment returned from AirChamber is correct*/
TEST_CASE("Zero Moment Calculation Airchamber (no lever)", "[AirChamber]")
{
    // init test parameters
    double radius = 1;       // in m
    double x = 0.0, y = 0.0; //  in m
    double pos[2] = {x, y};
    double pressure = 10e5; // in Pa
    double moment;
    // use air chamber class to compute wrench
    AirChamber test_chamb(radius, pos);
    test_chamb.setPressure(pressure);

    // result must be the same
    REQUIRE(test_chamb.getLever().cross(test_chamb.getForce()).norm() < NEARZERO);
}

/* check if update of pressure works */
TEST_CASE("Update check for ref of pressure", "[AirChamber]")
{

    // init test parameters
    double radius = 1;       // in m
    double pos[2] = {1, -1}; // in m
    double pressure = 10e5;  // in Pa
    Vector3d test_force[2];
    test_force[0].setZero();
    test_force[1].setZero();

    // use air chamber class to compute wrench
    AirChamber test_chamb(radius, pos);

    for (int iCase = 0; iCase < 2; iCase++)
    {
        test_chamb.setPressure(pressure);
        test_force[iCase] = test_chamb.getForce();
        pressure += 1000;
    }

    // results must differ
    REQUIRE(test_force[0] != test_force[1]);
}

/* check if dynamic alloc of AirChamber works */
TEST_CASE("Create rod element with airchambers", "[AirChamber]")
{
    // init test parameters
    double radRod = 0.6;
    double lengthRod = 12;
    int nNodes = 2;
    double radChamb = 0.001;          // in m
    double posChamb[2] = {0.1, -0.1}; // in m
    double pressure = 10e5;           // in Pa
    bool init = false;

    // attempt to initialize the rod element with airchambers
    try
    {
        AirChamber test_chamb(radChamb, posChamb);
        std::vector<AirChamber> chambers;
        chambers.push_back(test_chamb);
        RodElement test_rod(lengthRod, radRod, nNodes, true, chambers);
        init = true;
    }
    catch (const std::exception &e)
    {
        init = false;
        std::cout << e.what();
    }
    // initialisation needs to work
    REQUIRE(init);
}

// steiner is always computed with the assumption that xs = ys = 0
TEST_CASE("Test Steiner computation for single chamber", "[AirChamber]")
{
    double radChamb = 1;
    double posChamb[2] = {1, 1};
    AirChamber test_chamb(radChamb, posChamb);
    // expected results: Ixx = A*y^2, Iyy = A*x^2, Izz = Ip = Ixx+Iyy
    double Ixx = -M_PI * pow(radChamb, 2) * pow(posChamb[1], 2);
    double Iyy = -M_PI * pow(radChamb, 2) * pow(posChamb[0], 2);
    double Izz = Ixx + Iyy;
    Matrix3d expect = Matrix3d::Zero();
    expect.diagonal() << Ixx, Iyy, Izz;

    REQUIRE(test_chamb.getSteiner() == expect);
}

// steiner is always computed with the assumption that xs = ys = 0
TEST_CASE("Test 2nd moment of inertia computation for multiple chambers in rod", "[AirChamber]")
{
    double radChamb = 0.01;
    double radRod = 10;
    double lengthRod = 10;
    int nNodes = 2; // in m
    assert(cos(M_PI) == -1);
    double pos_ch_1[2] = {cos(180 / 360 * M_PI) * 1, sin(180 / 360 * M_PI) * 1}; // xy pos in m
    double pos_ch_2[2] = {cos(60 / 360 * M_PI) * 1, sin(60 / 360 * M_PI) * 1};   // xy pos in m
    double pos_ch_3[2] = {cos(300 / 360 * M_PI) * 1, sin(300 / 360 * M_PI) * 1}; // xy pos in m

    AirChamber test_chamb_1(radChamb, pos_ch_1);
    AirChamber test_chamb_2(radChamb, pos_ch_2);
    AirChamber test_chamb_3(radChamb, pos_ch_3);

    std::vector<AirChamber> chambers;

    chambers.push_back(test_chamb_1);
    chambers.push_back(test_chamb_2);
    chambers.push_back(test_chamb_3);

    std::cout << "---Create rod with air chambers:\n";
    RodElement test_rod(radRod, lengthRod, nNodes, true, chambers);
    test_rod.setMaterial().rho_ = 1; // set rho = 1, so that rhoJ = I;
    std::cout << "---Reinit:\n";
    test_rod.initDepParam(); // reinit. dep. parameters, since rho changed

    std::cout << "---Create rod without air chambers:\n";
    RodElement test_rod_no_chamb(radRod, lengthRod, nNodes, false);
    test_rod_no_chamb.setMaterial().rho_ = 1; // set rho = 1, so that rhoJ = I;
    std::cout << "---Reinit:\n";
    test_rod_no_chamb.initDepParam(); // reinit. dep. parameters, since rho changed
    // expected steiner: Ixx = A*y^2, Iyy = A*x^2, Izz = Ip = Ixx+Iyy

    double Ixx = -M_PI * pow(radChamb, 2) * (pow(pos_ch_1[1], 2) + pow(pos_ch_2[1], 2) + pow(pos_ch_3[1], 2));
    std::cout << "> Ixx test:\t" << Ixx << std::endl;
    double Iyy = -M_PI * pow(radChamb, 2) * (pow(pos_ch_1[0], 2) + pow(pos_ch_2[0], 2) + pow(pos_ch_3[0], 2));
    std::cout << "> Iyy test:\t" << Iyy << std::endl;
    double Izz = Ixx + Iyy;

    Matrix3d steiner = Matrix3d::Zero();
    steiner.diagonal() << Ixx, Iyy, Izz;

    Matrix3d expect = test_rod_no_chamb.getParam().rhoJ + steiner;
    Matrix3d result = test_rod.getParam().rhoJ;

    std::cout << "\n\n ---The residuum is: \n\n"
              << expect - result << std::endl;
    std::cout << "\t ... with relative Norm ||res-exp||/||steiner||:\t" << (result - expect).norm() / steiner.norm() << std::endl;
    std::cout << "\n\n and 2nd moment of inertia: \n\n"
              << result << std::endl;
    std::cout << "\n\n and steiner: \n\n"
              << steiner << std::endl;
    REQUIRE((result - expect).norm() / steiner.norm() < 100 * NEARZERO);
}

TEST_CASE("Test air chamber functionality (pressure)", "[AirChamber]")
{
    // init test parameters
    double radRod = 0.6;
    double lengthRod = 12;
    int nNodes = 2;
    double radChamb = 0.001;          // in m
    double posChamb[2] = {0.1, -0.1}; // in m
    double pressure = 10e5;           // in Pa
    // create chamber array
    AirChamber test_chamb(radChamb, posChamb);
    std::vector<AirChamber> chambers;
    chambers.push_back(test_chamb);

    // rod element with airchamber
    RodElement test_rod(lengthRod, radRod, nNodes, true, chambers);

    test_chamb.setPressure(pressure);
    test_rod.chamb_[0].setPressure(pressure);

    // assert
    REQUIRE(test_chamb.getForce() == test_rod.chamb_[0].getForce());
}


TEST_CASE("Test air chamber functionality (volume calculation)", "[AirChamber]")
{
    //arrange
    double radChamb = 1;            // in m
    double posChamb[2] = {1, 1};    // in m
    double ds = 0.1; // in m
    AirChamber test_chamb(radChamb, posChamb);

    MatrixXd Z = MatrixXd::Zero(2,6);
    Z << 0,0,1,0,0,0,0,0,1,0,0,0;
    double dV = (Z.block<1,3>(0,0)+Z.block<1,3>(0,3).cross(test_chamb.getLever())).norm()*test_chamb.getArea()*ds;
    //act
    test_chamb.setVolume();
    test_chamb.addPartialVolume(Z.block<1,3>(0,0) , Z.block<1,3>(0,3) , ds);
    test_chamb.display();
    // assert
    bool success = test_chamb.getVolume() == dV;
    REQUIRE(success);
}

TEST_CASE("Compute AirChamber Volume in Rod", "[ThermoDynamics]")
{
    //arrange
    double radChamb = 0.1;            // in m
    double posChamb[2] = {0, 0};    // in m
    AirChamber test_chamb(radChamb, posChamb);
    std::vector<AirChamber> chambers;
    chambers.push_back(test_chamb);

    double radRod = 0.2,lengthRod = 1; // in m
    RodElement rod(lengthRod,radRod,2,true,chambers);
    MatrixXd Z = MatrixXd::Zero(12,2);
    Z(8,0) = 1;
    Z(8,1) = 1;
    double V = test_chamb.getArea()*lengthRod;
    //act
    ThermoDynamics thermo;
    thermo.computeVolume(rod,Z);
    //assert
    REQUIRE( V == rod.chamb_[0].getVolume());
}

/*Test solver (static system) with Manipulator object. It is expected to elongate
under pressure. This validates indirectly, that the general behaviour is plausible
and no mistakes regarding signs are included.*/
TEST_CASE("Pressurise linear beam and test force", "[cosseratRodOde]")
{
    std::cout << "--- Pressure test in beam :" << std::endl << std::endl;
    // init test parameters (chose realistic parameters)
    double radRod = 0.02;
    double lengthRod = 0.12;
    int nNodes = 15;
    double radChamb = 0.004;         // in m
    double posChamb[2] = {0, 0}; // in m
    double pressure = 100000;         // in Pa

    // implement a single airchamber at the center of the rod.
    AirChamber test_chamb(radChamb, posChamb);
    std::vector<AirChamber> chambers;
    chambers.push_back(test_chamb);
    RodElement test_rod(lengthRod, radRod, nNodes, true, chambers);
    test_rod.setMaterial().rho_ = 0; // remove mass to neglect gravity
    test_rod.initDepParam();
    Manipulator SMR;
    SMR.addRodElement(test_rod);

    // actuate & compute solution
    SMR.RodElements()[0].chamb_[0].setPressure(pressure);
    Vector6d wrench = Vector6d::Zero();
    sleep(1);
    wrench = solveLevenbergMarquardt<objFunc<false>>(wrench, SMR);
    SMR.printState();

    // check if SMR is extending correctly
    double l_res = SMR.EE().norm();
    REQUIRE(l_res > lengthRod);
}

/*Test solver (static system) with beam element. Wrench has to be Zero vector,*/
TEST_CASE("Pressurise soft actuator from paper", "[cosseratRodOde]")
{
    std::cout << "--- Pressure test in beam :" << std::endl << std::endl;
    std::cout << "Assembling continuum robot!" << std::endl;
    Manipulator SMR;
// placement of airchambers as in Bartholdt et. al., 2021
    double rad_ch = 0.00515, dist_ch = 0.012; // in m
    
    double pos_ch_1[2] = {cos(180.0/180.0*M_PI)*dist_ch, sin(180.0/180.0*M_PI)*dist_ch}; // xy pos in m
    double pos_ch_2[2] = {cos(60.0/180.0*M_PI)*dist_ch, sin(60.0/180.0*M_PI)*dist_ch}; // xy pos in m
    double pos_ch_3[2] = {cos(300.0/180.0*M_PI)*dist_ch, sin(300.0/180.0*M_PI)*dist_ch}; // xy pos in m

    AirChamber chamb1(rad_ch, pos_ch_1);
    AirChamber chamb2(rad_ch, pos_ch_2);
    AirChamber chamb3(rad_ch, pos_ch_3);
    
    std::vector<AirChamber> chambers;
    chambers.push_back(chamb1);
    chambers.push_back(chamb2);
    chambers.push_back(chamb3);

    // define sections cap and actuator according to
    // Bartholdt et. al., 2021
    double rad = 0.021, l_tube = 0.11, l_off = 0.01;
    int nNodes_cap = 5, nNodes_actuator = 20;

    RodElement cap(l_off,rad, nNodes_cap, false);
    RodElement actuator(l_tube,rad, nNodes_actuator, true, chambers);

    cap.setMaterial().rho_ = 0; // exclude graviational loads
    cap.setMaterial().E_ = 320000;
    cap.setMaterial().G_ = 1.0667e5;
    cap.setMaterial().Be_ = 0;
    cap.setMaterial().Bs_ = 0;
    cap.setMaterial().Bt_ = 0;
    cap.setMaterial().Bb_ = 0;
    cap.initDepParam();

    actuator.setMaterial().rho_ = 0;
    actuator.setMaterial().Be_ = 0;
    actuator.setMaterial().Bs_ = 0;
    actuator.setMaterial().Bt_ = 0;
    actuator.setMaterial().Bb_ = 0;
    actuator.initDepParam();

    SMR.addRodElement(cap);
    SMR.addRodElement(actuator);
    SMR.addRodElement(cap);
    Vector6d wrench;
    wrench.setZero();
    double pressure[3] = {10000, 0, 0};
    SMR.RodElements()[1].chamb_[0].setPressure(pressure[0]);
    SMR.RodElements()[1].chamb_[1].setPressure(pressure[1]);
    SMR.RodElements()[1].chamb_[2].setPressure(pressure[2]);

    wrench = solveLevenbergMarquardt<objFunc<false>>(wrench, SMR);
    //SMR.printState(); //debug
    std::cout<<"Final wrench (static/without gravity): \t" << wrench.transpose() << std::endl;
    /* since no gravitational loads are included (rho_ = 0), the solution 
    has to be the zero vector*/
    REQUIRE(wrench.norm()<0.001); // the simulation only comes close to this values
}

/*Test objective funtion due to a bug that occured with Manipulator where inner_forces
are not const.*/
TEST_CASE("Test the inner forces if actuator is pressurized", "[cosseratRodOde]")
{
    Manipulator SMR;
// placement of airchambers as in Bartholdt et. al., 2021
    double rad_ch = 0.00515, dist_ch = 0.012; // in m
    
    double pos_ch_1[2] = {cos(180.0/180.0*M_PI)*dist_ch, sin(180.0/180.0*M_PI)*dist_ch}; // xy pos in m
    double pos_ch_2[2] = {cos(60.0/180.0*M_PI)*dist_ch, sin(60.0/180.0*M_PI)*dist_ch}; // xy pos in m
    double pos_ch_3[2] = {cos(300.0/180.0*M_PI)*dist_ch, sin(300.0/180.0*M_PI)*dist_ch}; // xy pos in m

    AirChamber chamb1(rad_ch, pos_ch_1);
    AirChamber chamb2(rad_ch, pos_ch_2);
    AirChamber chamb3(rad_ch, pos_ch_3);
    
    std::vector<AirChamber> chambers;
    chambers.push_back(chamb1);
    chambers.push_back(chamb2);
    chambers.push_back(chamb3);

    // define sections cap and actuator according to
    // Bartholdt et. al., 2021
    double rad = 0.021, l_tube = 0.11;
    int nNodes_actuator = 20;

    RodElement actuator(l_tube,rad, nNodes_actuator, true, chambers);

    actuator.setMaterial().rho_ = 0;
    actuator.setMaterial().Be_ = 0;
    actuator.setMaterial().Bs_ = 0;
    actuator.setMaterial().Bt_ = 0;
    actuator.setMaterial().Bb_ = 0;
    actuator.initDepParam();

    SMR.addRodElement(actuator);
    const int I_ACTUATOR = 0;
    Vector6d wrench;
    wrench.setZero();
    double pressure[3] = {10000, 0, 0};
    SMR.RodElements()[I_ACTUATOR].chamb_[0].setPressure(pressure[0]);
    SMR.RodElements()[I_ACTUATOR].chamb_[1].setPressure(pressure[1]);
    SMR.RodElements()[I_ACTUATOR].chamb_[2].setPressure(pressure[2]);

    wrench = objFunc<false>(wrench, SMR);
    MatrixXd inner_force = MatrixXd::Zero(3,nNodes_actuator);
    MatrixXd inner_moment = MatrixXd::Zero(3,nNodes_actuator);
    for(int i = 0; i<nNodes_actuator; i++)
    {
        Map<Vector3d> RTn(&inner_force(0,i));
        Map<Vector3d> n(&SMR.state()(7,i));
        std::cout << "Force \t" << n.transpose() << std::endl;
        Map<Quaterniond> h(&SMR.state()(3,i));
        std::cout << "\nRT\n" << h.toRotationMatrix().transpose() << std::endl;
        RTn = h.toRotationMatrix().transpose()*n;

        Map<Vector3d> RTm(&inner_moment(0,i));
        Map<Vector3d> m(&SMR.state()(10,i));
        RTm = h.toRotationMatrix().transpose()*m;
    }
    std::cout<< " \n\nINNER FORCE (except first node, this should be constant) \n\n" <<inner_force<<std::endl;
    std::cout<< " \n\nINNER MOMENT (except first node, this should be constant) \n\n" <<inner_moment<<std::endl;
    // inner force in Z have to be nearly constant in this example
    
    REQUIRE((inner_force.col(2)-inner_force.rightCols(1)).norm() < NEARZERO);
}
