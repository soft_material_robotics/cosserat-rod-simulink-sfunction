#define CATCH_CONFIG_MAIN
#define NEARZERO 1e-10
// external
#include <catch2/catch.hpp>
#include <Eigen/Dense>

// continuum robot lib (John Till)
#include <custom_math/custom_math.h>

// code base
#include <sim_lib/rod_element.h>
#include <sim_lib/air_chamber.h>
#include <sim_lib/cosserat_rod_quat.h>

#include <sim_lib/manipulator.h>
#include <sl_function/sl_func.h>
#include <sim_lib/objective_function.h>

#include <material_lib/ogden_neural_network.h>
// std libs
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <cmath>
#include <ctime>



/*Test rod ode (static system) with beam element*/
TEST_CASE("Test ODE standalone", "[cosseratRodOde]")
{
    std::cout<<"--- Test ODE Standalone "<<std::endl<<std::endl;
    bool success = true;
    double radRod = 0.010, lengthRod = 0.12;

    int nNodes = 50;
    double ds = lengthRod/(nNodes-1);
    Manipulator SMR;
    RodElement test_rod(radRod, lengthRod, nNodes, false);
    test_rod.setMaterial().rho_ = 0;
    test_rod.initDepParam();
    SMR.addRodElement(test_rod);

    Quaterniond h = Quaterniond::Identity();
    VectorXd y0(19);
    MatrixXd y(19, nNodes);
    MatrixXd z(19, nNodes);
    y.setZero();
    z.setZero();

    y0 << 0, 0, 0, h.coeffs(), 0, 0, 0, 0, 0, 0, Vector6d::Zero();
    y.col(0) << y0;
    try
    {
        for (int i = 0; i < test_rod.getResolution() - 1; i++)
        {
            cosseratRodOde(y0, Map<VectorXd>(&z(0, i), 12), Map<VectorXd>(&y(0, i), 19), test_rod);
            y.col(i + 1) = y.col(i) + ds * y0;
        }
    }
    catch (const std::exception &e)
    {
        e.what();
        success = false;
    }
    VectorXd tip_state = y.rightCols(1);
    REQUIRE(tip_state[2] - lengthRod < NEARZERO);
}

/*Test rod ode (static system) with beam element*/
TEST_CASE("Test ODE standalone (increase base force in z-dir)", "[cosseratRodOde]")
{
    std::cout<<"--- Test increase of base force "<<std::endl<<std::endl;
    bool success = true;
    double radRod = 0.010, lengthRod = 0.12;

    int nNodes = 50;
    double ds = lengthRod/(nNodes-1);
    Manipulator SMR;
    RodElement test_rod(radRod, lengthRod, nNodes, false);
    test_rod.setMaterial().rho_ = 0;
    test_rod.initDepParam();
    SMR.addRodElement(test_rod);

    Quaterniond h = Quaterniond::Identity();
    VectorXd y0(19);
    MatrixXd y(19, nNodes);
    MatrixXd z(19, nNodes);
    y.setZero();
    z.setZero();

    y0 << 0, 0, 0, h.coeffs(), 0, 0, 10, 0, 0, 0, Vector6d::Zero();
    y.col(0) << y0;
    try
    {
        for (int i = 0; i < test_rod.getResolution() - 1; i++)
        {
            cosseratRodOde(y0, Map<VectorXd>(&z(0, i), 12), Map<VectorXd>(&y(0, i), 19), test_rod);
            y.col(i + 1) = y.col(i) + ds * y0;
            // std::cout<<"n_"<<i+1<<"\t"<<Y.col(i).transpose()[14]<<"\t";
            // std::cout<<"n_s\t"<<y_s.transpose()[14]<<std::endl;
            // std::cout<<"M_"<<i<<"\t"<<Y.col(i).block<3,1>(15,0).transpose()<<std::endl<<std::endl;
        }
    }
    catch (const std::exception &e)
    {
        e.what();
        success = false;
    }
    VectorXd tip_state = y.rightCols(1);
    std::cout<< "Final length (def. state): " << tip_state[2] << std::endl;
    REQUIRE(tip_state[2] > lengthRod);
}

/*Test rod ode (static system) with beam element*/
TEST_CASE("Test ODE standalone (increase base moment in z-dir)", "[cosseratRodOde]")
{
    std::cout<<"\n\n\n\n\t--- Test increase of base moment "<<std::endl<<std::endl;
    bool success = true;
    double radRod = 0.010, lengthRod = 0.12;

    const int nNodes = 10;
    double ds = lengthRod/(nNodes-1);
    Manipulator SMR;
    RodElement test_rod(radRod, lengthRod, nNodes, false);
    test_rod.setMaterial().rho_ = 0;
    test_rod.initDepParam();
    SMR.addRodElement(test_rod);

    Quaterniond h = Quaterniond::Identity();
    VectorXd y0(19);
    MatrixXd y(19, nNodes);
    MatrixXd z(19, nNodes);
    y.setZero();
    z.setZero();

    y0 << 0, 0, 0, h.coeffs(), 0, 0, 0, 0, 0, 1, Vector6d::Zero(); // 1 Nm z-torque
    y.col(0) << y0;
    for (int i = 0; i < test_rod.getResolution() - 1; i++)
    {
            cosseratRodOde(y0, Map<VectorXd>(&z(0, i), 12), Map<VectorXd>(&y(0, i), 19), test_rod);
            y.col(i + 1) = y.col(i) + ds * y0;
            //Map<Quaterniond> h_norm( &y.col(i+1)[3]);
            std::cout << "\n\ny" << i+1 << ":\t" << y.col(i+1).block<4,1>(3,0).transpose()<< std::endl;
            //h_norm.normalize();
            std::cout << "y_norm" << i+1 << ":\t" << y.col(i+1).block<4,1>(3,0).transpose()<< std::endl;

    }

    VectorXd tip_state_1Nm = y.rightCols(1);
    double angle_1Nm = Quaterniond(tip_state_1Nm.block<4,1>(3,0)).angularDistance(Quaterniond::Identity())/M_PI*180;

    y0 << 0, 0, 0, h.coeffs(), 0, 0, 0, 0, 0, 2, Vector6d::Zero(); // 2 Nm z-torque
    y.col(0) << y0;
    for (int i = 0; i < test_rod.getResolution() - 1; i++)
    {
            cosseratRodOde(y0, Map<VectorXd>(&z(0, i), 12), Map<VectorXd>(&y(0, i), 19), test_rod);
            y.col(i + 1) = y.col(i) + ds * y0;
            Map<Quaterniond> h_norm( &y.col(i+1)[3]);
            std::cout << "\n\ny" << i+1 << ":\t" << y.col(i+1).block<4,1>(3,0).transpose()<< std::endl;
            h_norm.normalize();
            std::cout << "y_norm" << i+1 << ":\t" << y.col(i+1).block<4,1>(3,0).transpose()<< std::endl;
    }
    VectorXd tip_state_2Nm = y.rightCols(1);
    double angle_2Nm = Quaterniond(tip_state_2Nm.block<4,1>(3,0)).angularDistance(Quaterniond::Identity())/M_PI*180;
    
    std::cout<<"Tip's state 1Nm: " << tip_state_1Nm.block<13,1>(0,0).transpose() << std::endl;
    std::cout<<"Tip's state 10Nm: " << tip_state_2Nm.block<13,1>(0,0).transpose() << std::endl;
    std::cout<<"Total orientation state:\n " << y.block<4,nNodes>(3,0) << std::endl;

    
    REQUIRE(angle_2Nm - 2*angle_1Nm< NEARZERO);
}

// Different simple load cases
/*Test rod ode (static system) with beam element*/
TEST_CASE("Test ODE standalone (tip bending moment)", "[cosseratRodOde]")
{
    std::cout<<"\n\n\n--- Test ODE Standalone "<<std::endl<<std::endl;
    bool success = true;
    int nNodes = 400;
    Manipulator SMR;


    // test parameter
    double radRod = 1, lengthRod = 100;
    double E = 4/M_PI, G = 0.3*E; // in N/m²

    RodElement test_rod( lengthRod, radRod, nNodes, false);
    test_rod.setMaterial().rho_ = 0;
    test_rod.setMaterial().E_ = E;
    test_rod.setMaterial().G_ = G;
    test_rod.initDepParam();
    SMR.addRodElement(test_rod);

    // this is the analytical model to the plane bending problem
    double EI = E*pow(radRod,4)*M_PI/4; // chosen so that EI = 1

    // M = EI*kappa with kappa = 1/r and r = l0/theta:
    double M_by = EI*2*M_PI/lengthRod; // bending moment for full circle: theta= 2pi
    double x_EE_exp = 0; // tip matches origin
    double ext_moment[3] = {0, M_by, 0};
    SMR.setEEMomentExternal(ext_moment);
    // setup simulation
    VectorXd wrench(6);
    wrench << Vector6d::Zero();
    wrench[4] = M_by; // set initial guess to actual solution
    
    std::cout << "Compute ODE with bending moment..." << std::endl;
    wrench = solveLevenbergMarquardt<objFunc<false>>(wrench,SMR,10e-12);
    // assert
    VectorXd tip_state = SMR.EE();
    std::cout << "Base wrench\t" << wrench.transpose() << std::endl;
    std::cout << "Tip x-pos\t" << tip_state.transpose()<< std::endl;
    std::cout<<"Expected x-pos\t"<<x_EE_exp<<std::endl;
    REQUIRE(abs(tip_state[0] - x_EE_exp) < 0.00000001);
}

