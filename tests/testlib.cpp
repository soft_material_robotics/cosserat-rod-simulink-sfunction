#define CATCH_CONFIG_MAIN
#define NEARZERO 1e-10
// external
#include <catch2/catch.hpp>
#include <Eigen/Dense>

// continuum robot lib (John Till)
#include <custom_math/custom_math.h>

// code base
#include <sim_lib/rod_element.h>
#include <sim_lib/air_chamber.h>
#include <sim_lib/cosserat_rod_quat.h>

#include <sim_lib/manipulator.h>
#include <sl_function/sl_func.h>
#include <sim_lib/objective_function.h>

#include <material_lib/ogden_neural_network.h>
// std libs
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <cmath>
#include <ctime>



TEST_CASE("Matrix test", "[main]")
{
    MatrixXd test(3, 3);
    test.setIdentity();
    VectorXd testvec(9, 1);
    testvec.setZero();
    testvec << Eigen::Map<VectorXd>(test.data(), 9);
    VectorXd verify(9, 1);
    verify.setZero();
    verify << 1, 0, 0, 0, 1, 0, 0, 0, 1;
    REQUIRE(verify == testvec);
}

// Runtime test for the sl_io_func
TEST_CASE("Test startup of simulink block", "[main]")
{
    // init

    bool success = false;

    // call of implented functions as Simulink would
    try
    {
        SL_start_func();
        success = true;
    }
    catch (const std::exception &e)
    {
        throw(e);
        success = false;
    }

    REQUIRE(success); 
}

// Runtime test for the sl_io_func
TEST_CASE("Call feedforward network", "[main]")
{
    // init
    std::cout<< "Test Ogden Material Neural Network with n = [0 0 10]N: \t";
    bool success = false;
    double n [3] = {0,0,10};
    double v_ [3] = {0,0,0}; 
    clock_t t;

    // call of implented functions as Simulink would
    try
    {   
        t = clock(); 
  

        ogden_neural_network(n,v_);
        t = clock() - t;
        std::cout<<"Net Calculation Time: "<<((float)t)/CLOCKS_PER_SEC<<std::endl;
        Vector3d v = Vector3d(v_);
        std::cout<<"v "<<v.transpose()<<std::endl;
        success = true;
    }
    catch (const std::exception &e)
    {
        throw(e);
        success = false;
    }

    REQUIRE(success); 
}


// // Runtime test for the sl_io_func
// TEST_CASE("Test sl interface", "[main]")
// {
//     // init
//     std::cout << "--- Initialize Simulink block input" << std::endl;
//     bool success = false;
//     IN_type *in = nullptr;
//     OUT_type *out = nullptr;
//     // call of implented functions as Simulink would
//     clock_t t;
//     try
//     {
//         std::cout << "--- startup simulation" << std::endl;

//         SL_start_func();
//         for (int count = 0; count < 1; count++)
//         {
//             std::cout << "--- call io function" << std::endl;
//             t = clock();
//             SL_io_func(in, out);
//             t = clock() - t;
//             std::cout << "IO Time: " << ((float)t) / CLOCKS_PER_SEC << std::endl;
//         }
//         std::cout << "--- terminate simulation" << std::endl;

//         SL_terminate_func();
//         success = true;
//     }
//     catch (const std::exception &e)
//     {
//                     std::cout << "--- ERROR MESSAGE:" << std::endl;
//         std::cout << e.what() << std::endl;
//         success = false;
//     }

//     REQUIRE(success); 
// }
