
# Dependencies

Here is a list of dependencies required for the repository.

## GNU compiler and make:
```
sudo apt-get install build-essentials
```

## CMake
- Tested with CMake 3.22.3

[Source for install guide.](https://askubuntu.com/questions/355565/how-do-i-install-the-latest-version-of-cmake-from-the-command-line)

In some cases it might be necessary to reinstall CMake from scratch:
```
sudo apt purge --auto-remove cmake # remove current cmake version
```

Installation preparation
```
sudo apt update && \
sudo apt install -y software-properties-common lsb-release && \
sudo apt clean all
```

Obtain a copy of kitware's signing key.

```
wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | gpg --dearmor - | sudo tee /etc/apt/trusted.gpg.d/kitware.gpg >/dev/null
```


Add kitware's repository to your sources list for Ubuntu Focal Fossa (20.04), Ubuntu Bionic Beaver (18.04) and Ubuntu Xenial Xerus (16.04).

```
sudo apt-add-repository "deb https://apt.kitware.com/ubuntu/ $(lsb_release -cs) main"
```
As an optional step, is recommended that we also install the kitware-archive-keyring package to ensure that Kitware's keyring stays up to date as they rotate their keys.
```

sudo apt update
sudo apt install kitware-archive-keyring
sudo rm /etc/apt/trusted.gpg.d/kitware.gpg
```

Finally we can update and install the cmake package.

```
sudo apt update
sudo apt install cmake
```

## Doxygen
To generate documentation, [install doxygen](https://www.doxygen.nl/download.html) the required [dependencies](https://www.doxygen.nl/manual/install.html):

```
sudo apt install graphviz doxygen bison make python 
```
### Latex

```
sudo apt install texlive-latex-extra
```


## Matlab 2020b (only tested version)

[Website Mathworks](https://www.mathworks.com)
