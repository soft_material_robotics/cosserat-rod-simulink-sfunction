# Assembling actuators 

To change the actuators topology, meaning the number and type of 
serially-connected rods, the implemented classes are used.

- Goto [sl_func.cpp](../src/sl_func.cpp) and find the sl_start_func()

```C++
Manipulator *SMR();
void SL_start_func()
{
    SMR = new Manipulator();
    initParameters = false;
    step = 0;
```
A pointer to a Manipulator object is created.
We now start to assemble an actuator with three air chambers for
actuation, starting with the air chambers:
```C++
    // "assemble" manipulator

    // placement of airchambers as in Bartholdt et. al., 2021
    double rad_ch = 0.00515, dist_ch = 0.012; // radius in m and distance to center line in m
    double pos_ch_1[2] = {cos(180.0 / 180.0 * M_PI) * dist_ch, sin(180.0 / 180.0 * M_PI) * dist_ch}; // xy pos in m
    double pos_ch_2[2] = {cos(60.0 / 180.0 * M_PI) * dist_ch, sin(60.0 / 180.0 * M_PI) * dist_ch};   // xy pos in m
    double pos_ch_3[2] = {cos(300.0 / 180.0 * M_PI) * dist_ch, sin(300.0 / 180.0 * M_PI) * dist_ch}; // xy pos in m

    // create 3 independent AirChambers
    AirChamber chamb1(rad_ch, pos_ch_1); 
    AirChamber chamb2(rad_ch, pos_ch_2);
    AirChamber chamb3(rad_ch, pos_ch_3);

    // and add them to a STL container which is input to the RodElement constructor
    std::vector<AirChamber> chambers;
    chambers.push_back(chamb1);
    chambers.push_back(chamb2);
    chambers.push_back(chamb3);
```
We then continue with 2 different RodElement objects to define the actuated part
and caps made from different material (actuator: ecoflex(default), 
caps: dragonskin).
Also, the Marker-Plate for the optical tracking system is added as a thin, stiff
plate adding mass and rotational inertia to the system's tip.

```C++
    // define sections cap and actuator according to
    // Bartholdt et. al., 2021
    double rad = 0.021, rad_markerplate = 0.04, l_tube = 0.11, l_off = 0.010, l_mass = 0.00001;
    int nNodes_cap = 10, nNodes_actuator = 58, nNodes_mass = 2;

    RodElement cap(l_off, rad, nNodes_cap, false);
    RodElement actuator(l_tube, rad, nNodes_actuator, true, chambers);
    RodElement EE_mass(l_mass, rad, nNodes_mass, false);

    cap.setMaterial().rho_ = 1080;
    cap.setMaterial().E_ = 320000;
    cap.setMaterial().G_ = 1.0667e5;
    cap.setMaterial().Be_ = 0 * 0.01;
    cap.setMaterial().Bs_ = 0 * 0.01;
    cap.setMaterial().Bt_ = 0 * 0.01;
    cap.setMaterial().Bb_ = 0 * 0.01;
    cap.initDepParam();

    actuator.setMaterial().rho_ = 1070;
    actuator.setMaterial().Be_ = 0.0 * 0.01;
    actuator.setMaterial().Bs_ = 0.0 * 0.01;
    actuator.setMaterial().Bt_ = 0.0 * 0.01;
    actuator.setMaterial().Bb_ = 0.0 * 0.01;
    actuator.initDepParam();

    EE_mass.setMaterial().rho_ = 0.060 / (pow(rad, 2) * M_PI * l_mass); // total mass of 80g
    EE_mass.initDepParam();
```

Finally, all RodElement objects are added to the Manipulator in correct order:
```C++
    SMR->addRodElement(cap);
    SMR->addRodElement(actuator);
    SMR->addRodElement(cap);
    SMR->addRodElement(EE_mass);
    SMR->display();

    reaction_wrench = VectorXd::Zero(6); // init optimisation vector for shooting
    SMR->printState();

}
```
For a documentation of the classes, functions and members please refer to 
the automatically generate files in 
`out/build/docs/cosserat_rod_sfunction_doc/html` or compile the latex document
in `out/build/docs/cosserat_rod_sfunction_doc/latex` with 
```
./make_doc.sh
```

Read the 
[documentation on how to change inputs and outputs](how_to_change_simulation_inputs.md) 
of the system to see how a system may be actuated, and how you can change 
parameters parsed to the simulation.

