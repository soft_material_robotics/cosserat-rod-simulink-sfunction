# Change the simulations interface
The simulation communicates with any other application over three
pointers on structs defined by
- INPUT_type
- PARAMETER_type
- OUTPUT_type

in [sl_func.h](../include/sl_function/sl_func.h). 
Only basic C-compatible variables can be parsed. 

## General functionality 

The top-level source file for simulation is [sl_func.cpp](../src/sl_func.cpp).
The functions defined here are used as follows:

- The simulation is configured and started by the sl_start_func().
- At each time step the 
sl_io_func(INPUT_type *in, PARAMETER_type *param, OUTPUT_type *out)
is called and data is read and written to the structs referenced by the 
respective pointers.
- After the simulation has ended sl_stop_func() calls all destructors to free 
any allocated memory.

## Changing Inputs

- Goto [sl_func.h](../include/sl_function/sl_func.h)
- Add a new signal to INPUT_type, i.e., `double my_custom_signal[6]`

```C++
typedef struct{
    double pressure[3];
    double EEForceExternal[3];
    double EEMomentExternal[3];
    double my_custom_signal[6]; // new signal in input
} IN_type;
```

- Goto [sl_func.cpp](../src/sl_func.cpp)
- Define, what your signal is supposed to do in sl_io_func()

```C++
void SL_io_func(IN_type *ext_sim_in, PARAM_type *param, OUT_type *ext_sim_out){
    for(int i = 0; i < 6; i++)
    {
        std::cout << ext_sim_in->my_custom_signal[i] << std::endl; // i.e. print your signal
    }
}
```

Currently this is used to define pressure inputs to a pneumatic actuator like this:
```C++
void SL_io_func(IN_type *ext_sim_in, PARAM_type *param, OUT_type *ext_sim_out){
    /**
     * [...]
    */

    // set actuation pressure
    if (!(ext_sim_in == nullptr))
    {
        SMR->RodElements()[1].chamb_[0].setPressure(ext_sim_in->pressure[0]);
        SMR->RodElements()[1].chamb_[1].setPressure(ext_sim_in->pressure[1]);
        SMR->RodElements()[1].chamb_[2].setPressure(ext_sim_in->pressure[2]);
        
        SMR->RodElements()[1].chamb_[0].display();
        SMR->RodElements()[1].chamb_[1].display();
        SMR->RodElements()[1].chamb_[2].display();
    }
   /**
     * [...]
    */
```



## Changing Parameters

## Changing Outputs
Works just like changing the inputs. 
The state of the manipulator is completly exported like this:

```C++
void SL_io_func(IN_type *ext_sim_in, PARAM_type *param, OUT_type *ext_sim_out){
    /**
     * [...]
    */
 // process output
    if (!(ext_sim_out == nullptr))
    {
        Eigen::MatrixXd::Map(ext_sim_out->EE, 3, 1) = SMR->EE(); // set output
        // position of each node (Configuration)
        Eigen::MatrixXd::Map(ext_sim_out->rx, 1, TOTAL_RES) = SMR->state().block<1, TOTAL_RES>(INDEX_RX, 0);
        Eigen::MatrixXd::Map(ext_sim_out->ry, 1, TOTAL_RES) = SMR->state().block<1, TOTAL_RES>(INDEX_RY, 0);
        Eigen::MatrixXd::Map(ext_sim_out->rz, 1, TOTAL_RES) = SMR->state().block<1, TOTAL_RES>(INDEX_RZ, 0);
        // force of each node (Force)
        Eigen::MatrixXd::Map(ext_sim_out->nx, 1, TOTAL_RES) = SMR->state().block<1, TOTAL_RES>(INDEX_NX, 0);
        Eigen::MatrixXd::Map(ext_sim_out->ny, 1, TOTAL_RES) = SMR->state().block<1, TOTAL_RES>(INDEX_NY, 0);
        Eigen::MatrixXd::Map(ext_sim_out->nz, 1, TOTAL_RES) = SMR->state().block<1, TOTAL_RES>(INDEX_NZ, 0);
        // moments of each node (Moments)
        Eigen::MatrixXd::Map(ext_sim_out->mx, 1, TOTAL_RES) = SMR->state().block<1, TOTAL_RES>(INDEX_MX, 0);
        Eigen::MatrixXd::Map(ext_sim_out->my, 1, TOTAL_RES) = SMR->state().block<1, TOTAL_RES>(INDEX_MY, 0);
        Eigen::MatrixXd::Map(ext_sim_out->mz, 1, TOTAL_RES) = SMR->state().block<1, TOTAL_RES>(INDEX_MZ, 0);
        // orientation as quaternion
        Eigen::MatrixXd::Map(ext_sim_out->hi, 1, TOTAL_RES) = SMR->state().block<1, TOTAL_RES>(INDEX_HI, 0);
        Eigen::MatrixXd::Map(ext_sim_out->hj, 1, TOTAL_RES) = SMR->state().block<1, TOTAL_RES>(INDEX_HJ, 0);
        Eigen::MatrixXd::Map(ext_sim_out->hk, 1, TOTAL_RES) = SMR->state().block<1, TOTAL_RES>(INDEX_HK, 0);
        Eigen::MatrixXd::Map(ext_sim_out->h, 1, TOTAL_RES) = SMR->state().block<1, TOTAL_RES>(INDEX_H, 0);
        // translational velocity of each node (Moments)
        Eigen::MatrixXd::Map(ext_sim_out->qx, 1, TOTAL_RES) = SMR->state().block<1, TOTAL_RES>(INDEX_QX, 0);
        Eigen::MatrixXd::Map(ext_sim_out->qy, 1, TOTAL_RES) = SMR->state().block<1, TOTAL_RES>(INDEX_QY, 0);
        Eigen::MatrixXd::Map(ext_sim_out->qz, 1, TOTAL_RES) = SMR->state().block<1, TOTAL_RES>(INDEX_QZ, 0);
        // angular velocity of each node (Moments)
        Eigen::MatrixXd::Map(ext_sim_out->wx, 1, TOTAL_RES) = SMR->state().block<1, TOTAL_RES>(INDEX_WX, 0);
        Eigen::MatrixXd::Map(ext_sim_out->wy, 1, TOTAL_RES) = SMR->state().block<1, TOTAL_RES>(INDEX_WY, 0);
        Eigen::MatrixXd::Map(ext_sim_out->wz, 1, TOTAL_RES) = SMR->state().block<1, TOTAL_RES>(INDEX_WZ, 0);
        // copy material coordinates to output
        iMat = 0;
        for (auto s : SMR->s)
        {
            ext_sim_out->s[iMat] = s;
            iMat++;
        }
    }
    /**
     * [...]
    */
}

```
This way, every node and the respective discrete spatial domain s of the actuator can be stored into a mat-file.


