#!/bin/bash -e
BUILDDIR=out/build/
# rm -rf out
mkdir -p $BUILDDIR 
cmake -S . -B $BUILDDIR -DCMAKE_DUMMY=0
# This will create output on every line of cmake. Good for debugging of CMakeLists!
#cmake -S . -B $BUILDDIR --trace-source=CMakeLists.txt -DCMAKE_DUMMY=0
echo "Installing Library!"
cd out/build && sudo make install
# override the dummy library
cd ../..
#cp -v out/build/src/libgeneric_sl_interface.so scripts/custom_sfunction/build/

