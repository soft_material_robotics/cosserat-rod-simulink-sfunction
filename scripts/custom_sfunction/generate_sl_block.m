clear all
addpath('fcn')
import_bustypes

[folder, name, ext] = fileparts(which(mfilename));
builddir = fullfile(folder, 'build'); % build-Ordner im selben Verzeichnis wie dieses Skript
mkdir(builddir);
cd(builddir);

SRC_PATH = ['..',filesep,'..',filesep,'src',filesep,'sl_func_dummy.cpp'];
INCLUDE_PATH = ['..',filesep,'..',filesep,'include',filesep,'sl_function'];
try
    def = legacy_code('initialize')
    def.SourceFiles = {'../src/sl_func_dummy.cpp'};   % Die Angabe dieser Datei ist trotz der shared Library nötig, da diese ja nur mit ARM kompatibel ist.
    %def.SourceFiles = {'../../../src/sl_func_dummy.cpp'};   % Die Angabe dieser Datei ist trotz der shared Library nötig, da diese ja nur mit ARM kompatibel ist.
    def.HeaderFiles = {'sl_func.h'};
    def.IncPaths = {'../../../include/sl_function'};
    def.TargetLibFiles = {'libgeneric_sl_interface.so'};
    def.LibPaths = {'../build'};
    def.SFunctionName = 'external_simulation';
    
    def.StartFcnSpec = 'void SL_start_func()';
    def.OutputFcnSpec = 'void SL_io_func(IN_type u1[1],PARAM_type p1[1], OUT_type y1[1])';
    def.TerminateFcnSpec = 'void SL_terminate_func()';
    
    legacy_code('sfcn_cmex_generate', def);
    legacy_code('compile', def, '-DDUMMY');    % Erstellt MEX-File
    legacy_code('sfcn_tlc_generate', def);
    legacy_code('rtwmakecfg_generate', def);
    legacy_code('slblock_generate', def);
catch ME
    cd(folder)
    throw(ME)
end
cd(folder)
close_system('untitled',0)