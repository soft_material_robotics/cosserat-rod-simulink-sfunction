% This script loads required bus types automatically from header file,
% defining the interface. Replaces manually definition of inputs and
% outputs of the S-Function.
thispath = fileparts(mfilename("fullpath"));
cd(thispath)
Simulink.importExternalCTypes('../../include/sl_function/sl_func.h');

