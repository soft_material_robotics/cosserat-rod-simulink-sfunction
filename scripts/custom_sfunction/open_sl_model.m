clear
clc
% Path initialization
this_path = fileparts(which(mfilename));
cd(this_path);
addpath(fullfile(this_path));
addpath(fullfile(this_path,'build'));
addpath(fullfile(this_path,'fcn'));
% Define In- and Output-Buses
import_bustypes


% Open Simulink Model
open_system('./generic_test_model.mdl')
sl_parameter = Simulink.Parameter(robot_parameters);
sl_parameter.DataType = 'Bus: PARAM_type';
