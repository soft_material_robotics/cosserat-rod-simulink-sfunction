function sim_param = robot_parameters
%ROBOT_PARAMETERS Creates an structure containing mask parameters for the
%generated S-Function.
sim_param.damping_coeffs = [10,10,0.00001*0.7*12,0.04]; % shear, extension, bending, torsion
sim_param.stiffness_scaling = [2.2703,2.3841]; % scale extensional and bending stiffness
sim_param.isDynamic = 1 ;
sim_param.isHyperelastic  = 0;
end

