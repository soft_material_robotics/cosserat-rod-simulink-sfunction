#!/bin/bash -e
BUILDDIR=out/build/
#rm -rf out
mkdir -p $BUILDDIR
cmake -S . -B $BUILDDIR -DCMAKE_DUMMY=1 

# This will create output on every line of cmake. Good for debugging of CMakeLists!
# cmake -S . -B $BUILDDIR --trace-source=CMakeLists.txt

# Copy dummy results to matlab build folder
cd out/build && make
cd ../..
rm -rf scripts/build
mkdir -p scripts/custom_sfunction/build/
cp -v out/build/src/libgeneric_sl_interface.so scripts/custom_sfunction/build/

